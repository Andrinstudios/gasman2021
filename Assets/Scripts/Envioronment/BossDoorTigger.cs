using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using UnityEngine.Events;

public class BossDoorTigger : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{
    protected bool triggered = false;
    public float doorTimer = 4f;
    public UnityEvent triggerDoorOpen;
    public UnityEvent triggerDoorClose;
    public UnityEvent deactivatePlatforms;

    protected SpriteRenderer _renderer;

    private void Start()
    {
        _renderer = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "Player")
        {
            return;
        }

        if (triggered)
        {
            return;
        }

        _renderer.flipX = true;
        triggerDoorOpen.Invoke();
        triggered = true;
        StartCoroutine(StartDoorTimer());
    }


    protected IEnumerator StartDoorTimer()
    {
        yield return new WaitForSeconds(doorTimer);
        triggerDoorClose.Invoke();
       // deactivatePlatforms.Invoke();
        triggered = false;
        _renderer.flipX = false;
    }

    protected void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    protected void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {

        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.Respawn:
                triggered = false;
                break;

            default:
                break;
        }
    }
}
