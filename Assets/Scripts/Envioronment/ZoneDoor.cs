using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class ZoneDoor : ElevatorPlatform, MMEventListener<CorgiEngineEvent>
{

    public override void MoveTowardsEnd()
    {
        base.MoveTowardsEnd();
       // Debug.Log("moving to end");

    }

    public override void MoveTowardsStart()
    {
        base.MoveTowardsStart();
       // Debug.Log("Moving to start");
    }

    //this is overridden to prevent collision trigger from the base elevator class
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        return;
    }

    //this is overridden to prevent collision trigger from the base elevator class
    protected override void OnTriggerExit2D(Collider2D collision)
    {
        return;
    }

    protected virtual void ResetDoor()
    {
        atTop = false;
        ResetPath();
        
    }

    private void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {

        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.Respawn:
                ResetDoor();
                break;

            default:
                break;
        }
    }
}


