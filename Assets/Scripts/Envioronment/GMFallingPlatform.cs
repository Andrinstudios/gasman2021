using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;

public class GMFallingPlatform : FallingPlatform
{

    public float timeBeforeReset = 1f;
    public  MMFeedbacks startFalling;
   
    private bool _timerStarted = false;
    private bool _resetStarted = false;



    protected override void FixedUpdate()
    {
        if(_timerStarted)
        {
            _timer -= Time.deltaTime;
        }

        // we send our various states to the animator.		
        UpdateAnimator();

        if (_timer < 0)
        {
            _newPosition = new Vector2(0, -FallSpeed * Time.deltaTime);

            transform.Translate(_newPosition, Space.World);

            if (!_resetStarted)
            {
                StartCoroutine(ResetTimer());
                _resetStarted = true;
            }
        }


    }

    protected IEnumerator ResetTimer()
    {
        Debug.Log("Resetting platform");
        yield return new WaitForSeconds(timeBeforeReset);
        _resetStarted = false;

        DisableFallingPlatform();

    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
		CorgiController controller = collision.GetComponent<CorgiController>();
		if (controller == null)
			return;

        _timerStarted = true;
		_shaking = true;
	}

    public override void OnTriggerStay2D(Collider2D collider)
    {
        CorgiController controller = collider.GetComponent<CorgiController>();
        if (controller == null)
            return;
    }

    protected override void OnTriggerExit2D(Collider2D collider)
    {
        CorgiController controller = collider.GetComponent<CorgiController>();
        if (controller == null)
            return;
    }

    protected override void OnDisable()
    {
        _timerStarted = false;
        base.OnDisable();
    }
}
