using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class ZoneExitCriteria : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{ 
  //  [MMInformation("Enemies that need to be killed to open door.  Be sure to add the 'Exit Updater' scipt to each one and set the UnityEvent to the object ExitCriteria and trigger the DeathCounter method.", MoreMountains.Tools.MMInformationAttribute.InformationType.Info, false)]

    public List<Character> enemyList;
    public List<ZoneDoor> exitDoors;
    protected int counter = 0;


    public virtual void EnemyDeath()
    {
        counter++;
        if(counter == enemyList.Count)
        {
            OpenDoor();
        }

    }

    protected virtual void OpenDoor()
    {
        
        foreach (ZoneDoor exitDoor in exitDoors)
        {
            if (!exitDoor.atTop)
            {
                exitDoor.MoveTowardsEnd();
            }
            else
            {
                exitDoor.MoveTowardsStart();
            }
            
        }  
    }

    private void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {

        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.Respawn:
                counter = 0;
                break;

            default:
                break;
        }
    }

}
