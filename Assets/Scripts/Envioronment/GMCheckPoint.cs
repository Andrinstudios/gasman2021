﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;

public class GMCheckPoint : CheckPoint
{
	public MMFeedbacks checkpointReachedFeedback;


    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        base.OnTriggerEnter2D(collider);
		checkpointReachedFeedback.PlayFeedbacks();
    }

    protected override void OnDrawGizmos()
    {
		return;
		#if UNITY_EDITOR

		if (LevelManager.Instance == null)
		{
			return;
		}

		if (LevelManager.Instance.Checkpoints == null)
		{
			return;
		}

		if (LevelManager.Instance.Checkpoints.Count == 0)
		{
			return;
		}

		for (int i = 0; i < LevelManager.Instance.Checkpoints.Count; i++)
		{
			// we draw a line towards the next point in the path
			if ((i + 1) < LevelManager.Instance.Checkpoints.Count)
			{
				if (LevelManager.Instance.Checkpoints[i] != null && LevelManager.Instance.Checkpoints[i + 1] != null)
				{
					Gizmos.color = Color.green;
					Gizmos.DrawLine(LevelManager.Instance.Checkpoints[i].transform.position, LevelManager.Instance.Checkpoints[i + 1].transform.position);
				}
			}
		}
		#endif
	}

}
