using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class LightFlicker : MonoBehaviour
{
    public Light2D light;
    public float frequency = 10f;
    public float maxIntensity = 1.2f;
    public float minIntensity = 0.8f;

    protected bool changeIntensity = true;

    private void Update()
    {
        if (changeIntensity)
        {
            FlickerIntensity();
        }
        
    }

    protected virtual void FlickerIntensity()
    {
       float newIntensity = Random.Range(minIntensity, maxIntensity);
        light.intensity = newIntensity;
        StartCoroutine(FlickerTimer());
    }

    IEnumerator FlickerTimer()
    {
        changeIntensity = false;
        yield return new WaitForSeconds(1/frequency);
        changeIntensity = true;
    }
}
