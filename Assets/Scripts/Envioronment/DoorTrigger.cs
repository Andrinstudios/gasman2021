using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

public class DoorTrigger : MonoBehaviour,MMEventListener<CorgiEngineEvent>
{
    public UnityEvent triggerDoor;

    protected bool triggered = false;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "Player")
        {
            return;
        }

        if (triggered)
        {
            return;
        }

        triggerDoor.Invoke();
        triggered = true;
        
    }

    protected void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    protected void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {

        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.Respawn:
                triggered = false;
                break;

            default:
                break;
        }
    }
}
