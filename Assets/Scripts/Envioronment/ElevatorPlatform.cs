using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;

public class ElevatorPlatform : MMPathMovement
{
    public bool remotelyActivated = false;

    [Header("Feedbacks")]
    /// a feedback to trigger when a new point is reached
    [Tooltip("a feedback to trigger when a new point is reached")]
    public MMFeedbacks PointReachedFeedback;
    /// a feedback to trigger when the end of the path is reached
    [Tooltip("a feedback to trigger when the end of the path is reached")]
    public MMFeedbacks EndReachedFeedback;


    [Header("Debug")]

    [MMInspectorButton("ToggleMovementAuthorization")]
    public bool ToggleButton;
    [MMInspectorButton("ChangeDirection")]
    public bool ChangeDirectionButton;
    [MMInspectorButton("ResetEndReached")]
    public bool ResetEndReachedButton;
    [MMInspectorButton("MoveTowardsStart")]
    public bool MoveTowardsStartButton;
    [MMInspectorButton("MoveTowardsEnd")]
    public bool MoveTowardsEndButton;

 

    protected bool _scriptActivatedAuthorization = false;
    [HideInInspector]
    public bool atTop = false;
    protected bool _playerOnPlatform = false;
    protected float _platformTopY;
    protected const float _toleranceY = 0.05f;
    protected Collider2D _collider2D;
    protected CorgiController _collidingController = null;


    /// <summary>
    /// Initialization
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        CanMove = false;
        _collider2D = GetComponent<Collider2D>();
        PointReachedFeedback?.Initialization(this.gameObject);
        EndReachedFeedback?.Initialization(this.gameObject);
    }

    /// <summary>
    /// On Start we store our initial position
    /// </summary>
    protected override void Start()
    {
        base.Start();
    }

    protected override void EndReached()
    {
        base.EndReached();
        if (_currentIndex == 0)
        {
            atTop = true;
            CanMove = false;
            EndReachedFeedback?.PlayFeedbacks(this.transform.position);
        }
        else
        {
            CanMove = false;
        }
        
    }

    protected override void PointReached()
    {
        base.PointReached();
    }


    /// <summary>
    /// Forces the moving platform to move back to its start position
    /// </summary>
    public virtual void MoveTowardsStart()
    {
        CanMove = true;
        _endReached = false;
        _direction = -1;
        if (CurrentSpeed.magnitude > 0f)
        {
            _previousPoint = _currentPoint.Current;
            _currentPoint.MoveNext();
        }
        _scriptActivatedAuthorization = true;
    }

    /// <summary>
    /// Forces the moving platform to move to its end position
    /// </summary>
    public virtual void MoveTowardsEnd()
    {
        CanMove = true;
        _endReached = false;
        _direction = 1;
        if (CurrentSpeed.magnitude > 0f)
        {
            _previousPoint = _currentPoint.Current;
            _currentPoint.MoveNext();
        }
        _scriptActivatedAuthorization = true;
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (remotelyActivated)
        { return; }

        if (collision.tag != "Player")
        {
            return;
        }

        if (_playerOnPlatform)
        {
            return;
        }


        CorgiController controller = collision.GetComponent<CorgiController>();
        if (controller == null)
        {
            return;
        }


        _collidingController = controller;

        // if we're colliding with a character, we check that's it's actually above the platform's top
        _platformTopY = (_collider2D != null) ? _collider2D.bounds.max.y : this.transform.position.y;
        if (_collidingController.ColliderBottomPosition.y < _platformTopY - _toleranceY)
        {
            CanMove = false;
            return;
        }

        _playerOnPlatform = true;
        MoveTowardsEnd();
    }

    protected virtual void OnTriggerExit2D(Collider2D collision)
    {
        if (remotelyActivated)
        { return; }

        if (collision.tag != "Player")
        {
            return;
        }
        _playerOnPlatform = false;
        MoveTowardsStart();
    }

    
}

