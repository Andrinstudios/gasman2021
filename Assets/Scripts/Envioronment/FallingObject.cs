﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;

public class FallingObject : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{
    public bool requiresTrigger = false;
    public float timeBeforeFall = 1f;
    public float speed = 20f;

    public MMFeedbacks startingFeedback;
    public MMFeedbacks collidingFeedbacks;

    // private stuff
    protected Animator _animator;
    protected bool _shaking = false;
    protected bool _isFalling = false;
    protected Vector2 _newPosition;
    protected Bounds _bounds;
    protected Collider2D _collider2D;
    protected Vector3 _initialPosition;
   // protected float _timer;
    protected AutoRespawn _autoRespawn;
    protected LayerMask _triggerLayer;
    protected SpriteRenderer _renderer;
 

    // Start is called before the first frame update
    void Start()
    {
        Initialization();

        //bypass the trigger for use with the summoner enemy
        if (!requiresTrigger)
        {
            StartFall();
        }
    }

    protected virtual void Initialization()
    {
        _animator = GetComponent<Animator>();
        _collider2D = GetComponent<Collider2D>();
        _autoRespawn = GetComponent<AutoRespawn>();
        _bounds = LevelManager.Instance.LevelBounds;
        _initialPosition = this.transform.position;

        _renderer = gameObject.GetComponent<SpriteRenderer>();
        _triggerLayer = gameObject.GetComponent<DamageOnTouch>().TargetLayerMask;

        
       // _timer = timeBeforeFall;
    }

    protected virtual void FixedUpdate()
    {
        if (_isFalling)
        {
            _newPosition = new Vector2(0, -speed * Time.deltaTime);

            transform.Translate(_newPosition, Space.World);


            //spike is at the level bounds so destroy it.
            if (transform.position.y <= LevelManager.Instance.LevelBounds.min.y)
            {
                Deactivate();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (MMLayers.LayerInLayerMask(collision.gameObject.layer, _triggerLayer) && _isFalling)
        {
            HitTarget();
        }
    }

    //called from the FallingTrigger script
    public virtual void StartFall()
    {
        _shaking = true;
        if (_animator != null)
        {
            _animator.SetBool("Shaking", _shaking);
        }
        
        StartCoroutine(FallTimer());

    }

    public IEnumerator FallTimer()
    {
        yield return new WaitForEndOfFrame();
        startingFeedback?.PlayFeedbacks();
        yield return new WaitForSeconds(timeBeforeFall);
        _shaking = false;

        Fall();
    }

    public virtual void Fall()
    {
        if (_animator != null)
        {
            _animator.SetBool("Shaking", _shaking);
            _animator.SetTrigger("Falling");
        }
        _isFalling = true;
    }

    protected virtual void HitTarget()
    {
        startingFeedback?.PlayFeedbacks();
        Deactivate();
    }

    public virtual void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    public virtual void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    public virtual void OnMMEvent(CorgiEngineEvent cEvent)
    {
        if (cEvent.EventType == CorgiEngineEventTypes.Respawn)
        {
            Reset();
        }

        if (cEvent.EventType == CorgiEngineEventTypes.PlayerDeath)
        {
            Deactivate();
        }
    }

    protected virtual void Deactivate()
    {
        _renderer.enabled = false;
        _collider2D.enabled = false;
        StartCoroutine(ObjectDisableDelay());
    }

    protected virtual void Reset()
    {
        gameObject.SetActive(true);
        _renderer.enabled = true;
        _collider2D.enabled = true;
        _isFalling = false;
        _shaking = false;
        this.transform.position = _initialPosition;
        if (!requiresTrigger)
        {
            Destroy(this.gameObject);
        }
    }

    IEnumerator ObjectDisableDelay()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
        Destroy(this.gameObject);
    }
}
