using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class AiDecisionCounter : AIDecision
{
    public int minNumber = 3;
    public int maxNumber = 5;

    protected int iterations;
    protected int counter;

    public override void Initialization()
    {
        iterations = Random.Range(minNumber, maxNumber + 1);
        counter = 0;
    }


    public override bool Decide()
    {
        if (counter >= iterations)
        {
            ResetCounters();

            return true;
           
        }
        return false;
    }

    public override void OnEnterState()
    {
        base.OnEnterState();
        counter++;
        Debug.Log("Counter " + counter);
    }

    private void ResetCounters()
    {
        counter = 0;
        iterations = Random.Range(minNumber, maxNumber + 1);
    }

    public override void OnExitState()
    {
        Debug.Log("Exit state " + counter);
    }
}
