﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

public class AIDecisionHole : AIDecision
{

    /// The offset the hole detection should take into account
    public Vector3 HoleDetectionOffset = new Vector3(0, 0, 0);
    /// the length of the ray cast to detect holes
    public float HoleDetectionRaycastLength = 1f;
    protected Vector2 _direction;
    protected CorgiController _controller;
    protected Character _character;


    public override void Initialization()
    {
        _controller = GetComponent<CorgiController>();
        _character = GetComponent<Character>();
        base.Initialization();
    }

    public override bool Decide()
    {
        return HoleDetection();
    }

    private bool HoleDetection()
    {
        _direction = _character.IsFacingRight ? Vector2.right : Vector2.left;
        //Do something

        // we send a raycast at the extremity of the character in the direction it's facing, and modified by the offset you can set in the inspector.
        Vector2 raycastOrigin = new Vector2(transform.position.x + _direction.x * (HoleDetectionOffset.x + Mathf.Abs(GetComponent<BoxCollider2D>().bounds.size.x) / 2), transform.position.y + HoleDetectionOffset.y - (transform.localScale.y / 2));
        RaycastHit2D raycast = MMDebug.RayCast(raycastOrigin, -transform.up, HoleDetectionRaycastLength, _controller.PlatformMask | _controller.MovingPlatformMask | _controller.OneWayPlatformMask | _controller.MovingOneWayPlatformMask, Color.gray, true);
        // if the raycast doesn't hit anything
        if (!raycast)
        {
            Debug.Log("Found hole");
            return true;
        }
        else
        {
            return false;
        }

        
    }
}
