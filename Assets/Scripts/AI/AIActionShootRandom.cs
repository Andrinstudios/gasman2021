﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class AIActionShootRandom : AIAction
{
    /// if true, the Character will face the target (left/right) when shooting
    public bool FaceTarget = true;
    /// if true the Character will aim at the target when shooting
    public bool AimAtTarget = false;

    protected Character _character;
    protected CharacterHandleWeapon _characterHandleWeapon;
    protected CharacterHandleSecondaryWeapon _characterHandleSecondaryWeapon;
    protected WeaponAim _weaponAim;
    protected ProjectileWeapon _projectileWeapon;
    protected Vector3 _weaponAimDirection;
    protected int _numberOfShoots = 0;
    protected bool _shooting = false;
    [Tooltip("Time between shots")]
    public float minTime = 1;
    public float maxTime = 1;
    [Tooltip("0-1: 0 = Secondary, 1 = Primary")]
    [Range(0.0f,1.0f)]
    public float weightPrimaryWeapon = 1.0f;
    private bool timerTiming = false;


    /// <summary>
    /// On init we grab our CharacterHandleWeapon ability
    /// </summary>
    protected override void Initialization()
    {
        _character = GetComponent<Character>();
        _characterHandleWeapon = this.gameObject.GetComponent<CharacterHandleWeapon>();
        _characterHandleSecondaryWeapon = this.GetComponent<CharacterHandleSecondaryWeapon>();
    }

    /// <summary>
    /// On PerformAction we face and aim if needed, and we shoot
    /// </summary>
    public override void PerformAction()
    {
        if (!timerTiming)
        {
            StartCoroutine(ShootTimer());
        }
    }

    /// <summary>
    /// Sets the current aim if needed
    /// </summary>
    protected virtual void Update()
    {
        if (_characterHandleWeapon.CurrentWeapon != null)
        {
            if (_weaponAim != null)
            {
                if (_shooting)
                {
                    _weaponAim.SetCurrentAim(_weaponAimDirection);
                }
                else
                {
                    if (_character.IsFacingRight)
                    {
                        _weaponAim.SetCurrentAim(Vector3.right);
                    }
                    else
                    {
                        _weaponAim.SetCurrentAim(Vector3.left);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Faces the target if required
    /// </summary>
    protected virtual void TestFaceTarget()
    {
        if (!FaceTarget)
        {
            return;
        }

        if (this.transform.position.x > _brain.Target.position.x)
        {
            _character.Face(Character.FacingDirections.Left);
        }
        else
        {
            _character.Face(Character.FacingDirections.Right);
        }            
    }

    /// <summary>
    /// Aims at the target if required
    /// </summary>
    protected virtual void TestAimAtTarget()
    {
        if (!AimAtTarget)
        {
            return;
        }

        if (_characterHandleWeapon.CurrentWeapon != null)
        {
            if (_weaponAim == null)
            {
                _weaponAim = _characterHandleWeapon.CurrentWeapon.gameObject.MMGetComponentNoAlloc<WeaponAim>();
            }

            if (_weaponAim != null)
            {
                if (_projectileWeapon != null)
                {
                    _projectileWeapon.DetermineSpawnPosition();
                    _weaponAimDirection = _brain.Target.position - (_projectileWeapon.SpawnPosition);
                }
                else
                {
                    _weaponAimDirection = _brain.Target.position - _characterHandleWeapon.CurrentWeapon.transform.position;
                }                    
            }                
        }
    }

    /// <summary>
    /// Activates the weapon
    /// </summary>
    protected virtual void ShootPrimary()
    {
        if (_numberOfShoots < 1)
        {
            _characterHandleWeapon.ShootStart();
            _numberOfShoots++;
        }
    }

    protected virtual void ShootSecondary()
    {
        if (_numberOfShoots < 1)
        {
            _characterHandleSecondaryWeapon.ShootStart();
            _numberOfShoots++;
        }
    }

    /// <summary>
    /// When entering the state we reset our shoot counter and grab our weapon
    /// </summary>
    public override void OnEnterState()
    {
        base.OnEnterState();
        _numberOfShoots = 0;
        _shooting = true;
        _weaponAim = _characterHandleWeapon.CurrentWeapon.gameObject.MMGetComponentNoAlloc<WeaponAim>();
        _projectileWeapon = _characterHandleWeapon.CurrentWeapon.gameObject.MMGetComponentNoAlloc<ProjectileWeapon>();
    }

    /// <summary>
    /// When exiting the state we make sure we're not shooting anymore
    /// </summary>
    public override void OnExitState()
    {
        base.OnExitState();
        _characterHandleWeapon.ShootStop();
        _characterHandleSecondaryWeapon.ShootStop();
        _shooting = false;
    }
    
    private void ChooseWeapon()
    {
        float choice = Random.Range(0.0f, 1.0f);
        if (choice < weightPrimaryWeapon)
        {
            _characterHandleWeapon.ShootStart();
        }else
        {
            _characterHandleSecondaryWeapon.ShootStart();
        }
        
    }

    IEnumerator ShootTimer()
    {
        timerTiming = true;
        float timer = Random.Range(minTime, maxTime);
        yield return new WaitForSeconds(timer);
        TestFaceTarget();
        TestAimAtTarget();
        ChooseWeapon();
        timerTiming = false;
    }
}
