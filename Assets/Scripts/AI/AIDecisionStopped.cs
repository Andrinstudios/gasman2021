﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class AIDecisionStopped : AIDecision
{
    protected CharacterHorizontalMovement _characterHorizontalMovement;

    public override void Initialization()
    {
        _characterHorizontalMovement = this.gameObject.GetComponent<CharacterHorizontalMovement>();
        base.Initialization();
    }

    public override bool Decide()
    {
        return CheckForSpeed();
    }

    protected bool CheckForSpeed()
    {
        if (_characterHorizontalMovement.MovementSpeed == 0)
        {
            return true;
        }
        return false;
    }
}
