﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class AIActionPatrolFlip : AIActionPatrolWithinBounds
{
    public void ReverseDirection()
    {
        _direction = -_direction;
    }
}
