﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class GM_AIActionPresent : AIAction
{
    Animator _animator;

    protected override void Start()
    {
        _animator = this.GetComponent<Animator>();
        base.Start();
    }

    public override void PerformAction()
        {
            TriggerAnimation();
        }

    public override void OnEnterState()
    {
        base.OnEnterState();
    }

    private void TriggerAnimation()
    {
        Debug.Log("animation");
      //  _animator.SetBool("present",true);
    }
}
