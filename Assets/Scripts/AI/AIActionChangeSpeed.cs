using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;


public class AIActionChangeSpeed : AIAction
{
    protected CharacterHorizontalMovement _horizMovement;

    public float speed = 5f;
    public float startDelay = 0.5f;
    public MMFeedbacks startFeedback;
    public MMFeedbacks stopFeedback;

    protected float _baseSpeed;  //storage of the base speed to return the character after it loses contact.

    protected override void Awake()
    {
        _horizMovement = GetComponent<CharacterHorizontalMovement>();
        _baseSpeed = _horizMovement.WalkSpeed;
        base.Awake();
    }

    public override void PerformAction()
    {
        startFeedback?.PlayFeedbacks();
        _horizMovement.MovementSpeed = speed;
    }

    public override void OnExitState()
    {
        stopFeedback?.PlayFeedbacks();
        _horizMovement.MovementSpeed = _baseSpeed;
        base.OnExitState();
    }

    IEnumerator ChangeSpeed()
    {
        // Be sure the starDelay = the dealy in the feedback       
        startFeedback?.PlayFeedbacks();

        yield return new WaitForSeconds(startDelay);

        _horizMovement.MovementSpeed = speed;
    }
}
