using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;


// overrides the MM verision to allow for setting the animation parameters

public class GMAiFlyerDetectTargetRadius : AIDecisionDetectTargetRadius
{
    public override bool Decide()
    {
        if (DetectTarget())
        {
            _character._animator.SetBool("Following", true);
        }
        else
        {
            _character._animator.SetBool("Following", false);
        }

        return base.Decide();
    }

}
