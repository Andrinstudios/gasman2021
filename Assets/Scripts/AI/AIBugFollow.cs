﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;


public class AIBugFollow : MonoBehaviour
{
    
    private GameObject _leader;

    protected CharacterHorizontalMovement _characterHorizontalMovement;
    protected CharacterRun _characterRun;
    protected CharacterJump _characterJump;
    protected Transform _leaderPosition;

    protected float _speed = 1f;
    protected float _direction;
    protected Vector2 _directionVector;
    protected CorgiController _controller;

    private float fudgeFactor = 0;

    public float _minDistanceFromLeader = 1f;
    public bool avoidFalling = true;
    public Vector3 HoleDetectionOffset = new Vector3(0, 0, 0);
    /// the length of the ray cast to detect holes
    public float HoleDetectionRaycastLength = 1f;


    private void Start()
    {
        _characterHorizontalMovement = GetComponent<CharacterHorizontalMovement>();
        _characterRun = GetComponent<CharacterRun>();
        _characterJump = GetComponent<CharacterJump>();
        _controller = GetComponent<CorgiController>();

        fudgeFactor = Random.Range(.7f, 1.3f);
        _characterHorizontalMovement.WalkSpeed = fudgeFactor * _characterHorizontalMovement.WalkSpeed;
        BeginMovement();

    }

    private void Update()
    {
        // check to see if there is a leader assigned
        if (_leaderPosition)
        {
            

            //get the distance to the leader
            float distance = Mathf.Abs(_leaderPosition.position.x - transform.position.x);

            //determine the direction to move
            _direction = _leaderPosition.position.x > transform.position.x ? 1f : -1f;

            //if we are at hole we want to tuirn around
            CheckForHoles();

            //test to see how far away we are from the leader.
            if (distance > (_minDistanceFromLeader * fudgeFactor))
            {
                _characterHorizontalMovement.SetHorizontalMove(_speed * _direction);
            }
        }
        
    }

    public void SetLeader(GameObject leader)
    {
        _leader = leader;
        _leaderPosition = leader.transform;
    }

    private void BeginMovement()
    {
        if (!_leaderPosition)
        {
            return;
        }

        //determine the direction to move and start the movement
        _direction = _leaderPosition.position.x > transform.position.x ? 1f : -1f;
        _characterHorizontalMovement.SetHorizontalMove(_speed * _direction);
    }


    protected virtual void CheckForHoles()
    {
        // if we're not grounded or if we're not supposed to check for holes, we do nothing and exit
     //   if (!avoidFalling || !_controller.State.IsGrounded)
     //   {
     //       return;
     //  }

        // we send a raycast at the extremity of the character in the direction it's facing, and modified by the offset you can set in the inspector.
        Vector2 raycastOrigin = new Vector2(transform.position.x + _directionVector.x * (HoleDetectionOffset.x + Mathf.Abs(GetComponent<BoxCollider2D>().bounds.size.x) / 2), transform.position.y + HoleDetectionOffset.y - (transform.localScale.y / 2));
        RaycastHit2D raycast = MMDebug.RayCast(raycastOrigin, -transform.up, HoleDetectionRaycastLength, _controller.PlatformMask | _controller.MovingPlatformMask | _controller.OneWayPlatformMask | _controller.MovingOneWayPlatformMask, Color.gray, true);
        // if the raycast doesn't hit anything
        if (!raycast)
        {
            // we change direction
           // Debug.Log(this.gameObject.name + " found hole");
            ChangeDirection();
        }
    }

    /// <summary>
    /// Changes the agent's direction and flips its transform
    /// </summary>
    protected virtual void ChangeDirection()
    {
        _direction = -_direction;
    }

}
