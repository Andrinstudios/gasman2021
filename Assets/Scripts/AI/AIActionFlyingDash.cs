using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class AIActionFlyingDash : AIAction
{

    protected FlyingDash _dash;

    protected override void Initialization()
    {
        _dash = GetComponent<FlyingDash>();
        base.Initialization();
    }

    public override void PerformAction()
    {
        _dash.StartDash();
    }
}
