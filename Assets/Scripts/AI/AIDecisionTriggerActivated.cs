using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;


public class AIDecisionTriggerActivated : AIDecision
{

    protected GMCharacterSummon _summon;


    public override void Initialization()
    {
        _summon = GetComponent<GMCharacterSummon>();
        if (_summon == null)
        {
            Debug.Log("DID not find summoner");
        }
    }

    public override bool Decide()
    {
        
        if (_summon.isTriggered)
        {
            return true;
        }
        return false;
    }

}
