using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

public class AIActionShockwave : AIAction
{
    protected ShockwaveAttack shockwaveAttack;


    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        shockwaveAttack = GetComponent<ShockwaveAttack>();
    }

    public override void PerformAction()
    {
        shockwaveAttack.Shoot();
    }

}
