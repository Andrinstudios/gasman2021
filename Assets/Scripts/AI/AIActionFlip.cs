﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

public class AIActionFlip : AIAction
{
    protected AIActionPatrolFlip _movement;
    protected Character _character;
    protected Vector2 _direction;

    protected override void Start()
    {
        _movement = GetComponent<AIActionPatrolFlip>();
        //_character = GetComponent<Character>();
    }


    public override void PerformAction()
    {
        Flip();
    }

    protected virtual void Flip()
    {
        _movement.ReverseDirection();
        
    }
}
