using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

public class AIActionShield : AIAction
{
    protected ShellEnemy _shell;

    protected override void Awake()
    {
        _shell = this.gameObject.GetComponentInChildren<ShellEnemy>();
        base.Awake();
    }

    public override void PerformAction()
    {
        _shell.DeployShell();
    }
}
