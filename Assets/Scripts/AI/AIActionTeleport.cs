using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

public class AIActionTeleport : AIAction
{
    protected GMEnemyTeleporter _teleporter;

    protected override void Awake()
    {
        base.Awake();
        _teleporter = GetComponent<GMEnemyTeleporter>();
    }



    public override void PerformAction()
    {
        if (_teleporter == null)
        {
            return;
        }
        Debug.Log("calling teleport");
        _teleporter.Teleport();
    }
}
