﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Feedbacks;
using MoreMountains.CorgiEngine;


[AddComponentMenu("")]
[FeedbackPath("Sounds/Random Sound")]
[FeedbackHelp("This feedback lets you play a random sound through the Corgi Engine's SoundManager")]

public class GMFeedBackRandonSound : MMFeedback
{
    public List<AudioClip> soundList;
    public bool Loop = false;
    public bool pitchShiftShound = false;
    

    protected AudioSource _audioSource;

    protected override void CustomPlayFeedback(Vector3 position, float attenuation = 1)
    {
        if (Active)
        {
            if (soundList != null)
            {
                int index = Random.Range(0, soundList.Count);
                AudioClip _audioClip = soundList[index];

                if (!pitchShiftShound)
                {
                    _audioSource = SoundManager.Instance.PlaySound(_audioClip, transform.position, Loop);
                    return;
                }

                //add random pitch variation to the sound
                float pitchShift = Random.Range(0.75f, 1.25f);
                _audioSource = SoundManager.Instance.PlaySound(_audioClip, transform.position, pitchShift, 0f, 0f, 1f, Loop);
                
            }
        }
    }

    protected override void CustomStopFeedback(Vector3 position, float attenuation = 1)
    {
        if (Loop)
        {
            SoundManager.Instance.StopLoopingSound(_audioSource);
        }
        _audioSource = null;
        
    }
}

