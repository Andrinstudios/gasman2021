﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class GMCharacterForwardJump : CharacterJump
{
    public float forwardDistance = 4;


    public override void JumpStart()
    {
        base.JumpStart();
        _controller.SetHorizontalForce(forwardDistance);
    }

}
