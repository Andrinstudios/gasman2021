﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class BugHealth : Health //, MMEventListener<CorgiEngineEvent>
{
    // Start is called before the first frame update
    [MMReadOnly]
    public bool justCreated = true;

    //Amount of time after instancing to be proteced
    public float invulnerableTime = 2f;
    [HideInInspector]
    private GameObject leader;


    private void Awake()
    {
        //register with the sceneDataHolder
        SceneDataHolder _sceneData = FindObjectOfType<SceneDataHolder>();
        GMSaveLoad saveable = GetComponent<GMSaveLoad>();

        _sceneData.AddEnemyToSaveableList(saveable);
 
    }

    protected override void OnEnable()
    {
        GMProximityManager pManager = FindObjectOfType<GMProximityManager>();
        pManager.AddManagedObect(GetComponent<ProximityManaged>());

        //this.MMEventStartListening<CorgiEngineEvent>();
        //if (justCreated)
        //{
        //    Debug.Log("Enable timer " + gameObject.name);
        //    DamageDisabled();
        //     StartCoroutine(CreatedTimer());
        //     CurrentHealth = InitialHealth;
        //}
       
       // DamageDisabled();
        UpdateHealthBar(false);
       
    }

    //public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    //{
    //    switch (thisEvent.EventType)
    //    {
    //        case CorgiEngineEventTypes.Respawn:
    //            justCreated = true;
    //            break;
    //        default:
    //            break;
    //    }
    //}

    ////Timer to prevent killing of bug before it can be created
    ////Gives time for animations to run and previous gas cloud to celar.
    ////Needs clear feedback to player that it is not damageable during this time.

    //IEnumerator CreatedTimer()
    //{
    //    yield return new WaitForSeconds(2f);
    //    DamageEnabled();
    //    justCreated = false;
    //}

    protected override void DestroyObject()
    {
        LeaderCounter _leaderCounter = leader.GetComponent<LeaderCounter>();
        _leaderCounter.AdvanceKillCounter();
        base.DestroyObject();
    }

    public void AssignLeader(GameObject _leader)
    {
        leader = _leader;
    }

    public override void Revive()
    {
       // Debug.Log("follower revive");
        base.Revive();
        DamageDisabled();
        if (isActiveAndEnabled)
        {
            StartCoroutine(DamageEnabled(3f));
        }
        
    }

    public override void DamageDisabled()
    {
       // Debug.Log("Damage disabled");
        base.DamageDisabled();
    }
}
