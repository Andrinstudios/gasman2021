﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;


/*TODO
 * Make direction face same as character
 * disable collision with enemy holding device when using and facing right direction 
 * add rotation
 * add soundFX
 * add particles??
 * add ability to transform and shoot back at player??
*/
public class VacuumScript : MonoBehaviour {

    public float vacuumForce = 75;
    public float maxGasLevel = 50;

    public bool isChargableWeapon = false;
    public float currentGasLevel;

    public float weaponDamage = 10;

    public AudioClip activeSound;
    public AudioClip weaponChargedSound;
    public AudioClip fireSound;

    private Character _character;
    private bool isCharged = false;
    private bool isInWeaponState = false;


    private void Start()
    {
        _character = GetComponentInParent<Character>();
        currentGasLevel = 0f;

    }

    private void Update()
    {
        //adjust the direction of the vaccuum
        if (_character.IsFacingRight)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }else
        {
            transform.localScale = new Vector3(1, 1, 1);
        }

        if (isCharged && !isInWeaponState)
        {
            BeginWeaponState();
        }

    }

    private /// <summary>
    /// OnParticleCollision is called when a particle hits a collider.
    /// </summary>
    /// <param name="other">The GameObject hit by the particle.</param>
    void OnParticleCollision(GameObject other)
    {
        Debug.Log("collided "+other.name);
    }

     private void OnParticleTrigger()
    {
        Debug.Log("trigger ");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("entered "+collision.gameObject.name);
        //test to see is this a vaccumable type.
        CloudProjectile _cloud = collision.gameObject.GetComponent<CloudProjectile>();
        if (_cloud == null)
        {
            return;
        }

        //set flag to inVacuum
        _cloud.inVacuum = true;

        Animator anim = collision.gameObject.GetComponent<Animator>();
        anim.SetBool("InVacuum", true);


        //move the sprite to transform
        Rigidbody2D rb = collision.gameObject.GetComponent<Rigidbody2D>();
        Vector3 moveDirection = transform.position - collision.transform.position;
        rb.AddForce(moveDirection*vacuumForce);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        CloudProjectile _cloud = collision.gameObject.GetComponent<CloudProjectile>();

        // prevent cloud from being destroyed if approaching from wrong side.
        if (_cloud.inVacuum == false) { return; }
        DamageOnTouch _damage = _cloud.GetComponent<DamageOnTouch>();
        AddGas(_damage.DamageCaused);


        _cloud.enabled = false;
    }

    void AddGas(float amount)
    {
        currentGasLevel += amount;
    
        if (currentGasLevel >= maxGasLevel)
        {
            currentGasLevel = maxGasLevel;
            isCharged = true;
        }

        //TODO add update to characters sprite to show tank filling
    }

    void RemoveGas (float amount)
    {
        currentGasLevel -= amount;
        isCharged = false;
        if (currentGasLevel <= 0)
        {
            currentGasLevel = 0f;
        }
    }

    void BeginWeaponState()
    {
        isInWeaponState = true;
    }

    void EndWeaponState()
    {
        isInWeaponState = false;
    }

    void FireWeapon()
    {
        isInWeaponState = false;
        RemoveGas(currentGasLevel);
    }

    void UpdateTankMeter()
    {
        
    }
}
