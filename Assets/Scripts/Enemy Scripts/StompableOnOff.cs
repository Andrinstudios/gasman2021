using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;

public class StompableOnOff : MonoBehaviour
{
    public float intervalTime = 1.5f;

    // be sure the feedbacks are times to match the intervalTime above or there will be some strange behaviors.
    public MMFeedbacks BouncerOn;
    public MMFeedbacks BouncerOff;

    protected Stompable _stompable;
    protected bool timerRunning = true;
    

    private void Awake()
    {
        _stompable = GetComponent<Stompable>();
    }

    private void Update()
    {
        if (timerRunning)
        {
            StartCoroutine(ToggleStomp());
            
            _stompable.enabled = !_stompable.enabled;

            if (_stompable.enabled)
            {
                BouncerOn?.PlayFeedbacks();
            }
            else
            {
                BouncerOff?.PlayFeedbacks();
            }

            timerRunning = false;
        }
    }

    IEnumerator ToggleStomp()
    {
        yield return new WaitForSeconds(intervalTime);
        timerRunning = true; ;
    }
}
