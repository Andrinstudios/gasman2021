﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;

public class Laser : MonoBehaviour
{

    public GameObject laserAttachment; 
    public float timeOn;
    public float timeOff;
    public LayerMask laserMask;
    public LayerMask killMask;

    public LineRenderer laserLineRenderer;
    public float lineSegmentLength;  // determines the number of segments in the line.
    public float minLineVariation =0f;
    public float maxLineVariation = 0f;

    public MMFeedbacks contactFeedbacks;
    public MMFeedbacks startFeedbacks;
    public MMFeedbacks originFeedbacks;


    protected bool isFiring = false;
    protected bool canFire = true;
    protected bool isVisible = false;
    protected Vector2 contactPoint;
    protected float rayLength;

    protected LaserDamageOnTouch _damageOnTouch;

    public bool isSwitchControlled = false;

    protected virtual void OnDisable()
    {
        if (isSwitchControlled) { return; }
        StopCoroutine(FireLaser());
        isFiring = false;
        canFire = true;

    }

    private void Awake()
    {
        _damageOnTouch = GetComponent<LaserDamageOnTouch>();
        if (isSwitchControlled)
        {
            canFire = false;
        }
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (canFire && !isFiring)
        {
            startFeedbacks?.PlayFeedbacks(laserAttachment.transform.position, 1f);

            if (isSwitchControlled)
            {
                isFiring = true;
                laserLineRenderer.enabled = true;
            }
            else
            {
                StartCoroutine(FireLaser());
            }

        }

        

        if (canFire)
        {
            if (isSwitchControlled)
            {
                StartLaser();
            }
            else
            {
                CastLaserRay();
                DrawLaser();
            }
        }
    }


    // public method for UnityEvents to activate from switch
    public virtual void TurnOnLaser()
    {
        canFire = true;

    }

    public virtual void TurnOffLaser()
    {
        canFire = false;
        isFiring = false;
        StopLaser();
    }


    //Coroutine for timer based laser
    IEnumerator FireLaser()
    {
        isFiring = true;
        if (originFeedbacks)
        {
            originFeedbacks.PlayFeedbacks(laserAttachment.transform.position, 1f);
        }

        laserLineRenderer.enabled = true;

        yield return new WaitForSeconds(timeOn);

        isFiring = false;
        canFire = false;
        StartCoroutine(HoldFire());
    }

    IEnumerator HoldFire()
    {
        if(startFeedbacks)
        {
            startFeedbacks.StopFeedbacks();
        }
        if (originFeedbacks)
        {
            originFeedbacks.StopFeedbacks();
        }

        laserLineRenderer.enabled = false;
        contactFeedbacks.StopFeedbacks();
        yield return new WaitForSeconds(timeOff);
        canFire = true;
    }

    protected virtual void CastLaserRay()
    {
        Vector3 _rayDirection = laserAttachment.transform.position - this.transform.position;

        RaycastHit2D _laserRay = MMDebug.RayCast(laserAttachment.transform.position, _rayDirection, Mathf.Infinity, laserMask, Color.red, true);
        if (_laserRay)
        {
            contactPoint = _laserRay.point;
            contactFeedbacks?.PlayFeedbacks(contactPoint, 1f);
            rayLength = _laserRay.distance;

            if (MMLayers.LayerInLayerMask(_laserRay.collider.gameObject.layer, killMask))
            {
                // Do soemthing here to damage the player
                _damageOnTouch.HitByRay(_laserRay.collider);

            }

        }
        else
        {
            contactFeedbacks.StopFeedbacks();
        }
    }

    protected virtual void DrawLaser()
    {

        //break up the line in to segments by addign points along the ray to be used for variability if needed

        if (lineSegmentLength > 0)  //check to be sure we are breakion up the line (0 = no segments)
        {

            int segments = (int)(rayLength / lineSegmentLength);

            laserLineRenderer.positionCount = segments + 1;



            for (int i = 1; i < segments; i++)
            {
                Vector3 nextPosition = Vector3.Lerp(laserAttachment.transform.position, contactPoint, (float)i / segments);
                Vector3 randomVector = new Vector3(Random.Range(minLineVariation, maxLineVariation), Random.Range(minLineVariation, maxLineVariation), 1);

                //  Vector3 segPosition = new Vector3(xPos - segmentLength, laserAttachment.transform.position.y + Random.Range(-0.5f,1.1f), laserAttachment.transform.position.z);

                Vector3 segPosition = nextPosition + randomVector;
                laserLineRenderer.SetPosition(i, segPosition);
            }
            // these points are the fixed start and endpoint of the line renderer
            laserLineRenderer.SetPosition(0, laserAttachment.transform.position);
            laserLineRenderer.SetPosition(segments, contactPoint);
        }
        else
        {
            // there are no segements so only 2 points needed
            laserLineRenderer.SetPosition(0, laserAttachment.transform.position);
            laserLineRenderer.SetPosition(1, contactPoint);
        }
        

    }

    // on/off for laser with switch control
    protected virtual void StartLaser()
    {
        CastLaserRay();
        DrawLaser();
        originFeedbacks?.PlayFeedbacks(laserAttachment.transform.position, 1f);
    }

    protected virtual void StopLaser()
    {
        startFeedbacks?.StopFeedbacks();
        originFeedbacks?.StopFeedbacks();

        laserLineRenderer.enabled = false;
        contactFeedbacks?.StopFeedbacks();
    }


}
