﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTest : MonoBehaviour
{
 public Vector3 from;
 public Vector3 to   = new Vector3(0f, 0f, 225f);
 Vector3 endingSpot = new Vector3(0f,0f,0f);
 public float speed = 2.0f; 
 public float cycleTime = 3f;
 public float returnTime = 1.5f;
 private float timer = 0.0f;
 bool isRotating = false;
 
private void Start()
{
    from = new Vector3(transform.rotation.x, transform.rotation.y, transform.rotation.z);
    to = new Vector3(transform.rotation.x, transform.rotation.y, transform.rotation.z + 75f);
}
 void Update() 
 {
    if( Input.GetKeyDown("space") && !isRotating)
     {
         timer = 0;
         isRotating = true;
         StartCoroutine(RotateMe());
     }
    
        // float t = Mathf.PingPong(Time.time * speed * 2.0f, 1.0f);
 }

    IEnumerator RotateMe()
    {
        Debug.Log("rotating");
        float xTime = 0;
        while (timer < 1)
        {
           // float t = (Mathf.Sin (xTime * speed * Mathf.PI * 2.0f) + 1.0f) / 2.0f;
            float t = Mathf.Abs( Mathf.Sin (xTime * speed * Mathf.PI));
            Debug.Log("t= "+xTime * speed * Mathf.PI);
            transform.eulerAngles = Vector3.Lerp (from, to, t);
            timer  += Time.deltaTime/cycleTime;
            xTime += Time.deltaTime;
            yield return null;
        }
        isRotating = false;
    
    }

    IEnumerator ReturnToStart()
    {
        float i = 0f;
        Debug.Log("returning");
        while (timer < 1)
        {   
            timer += Time.deltaTime/returnTime;
    
            transform.eulerAngles = Vector3.Lerp(endingSpot, from, timer);
            
            yield return null;
        }
        to = new Vector3(transform.rotation.x, transform.rotation.y, transform.rotation.z + 75f);
        timer = 0;
        isRotating = false;
    }
   

}

