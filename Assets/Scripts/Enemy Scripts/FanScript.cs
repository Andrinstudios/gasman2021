﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanScript : MonoBehaviour {

	public float fanForce = 10f;
	public LayerMask _layerMask;

	private bool isBeingBlown = false;
	private Vector2 force;

	void Start () {
		force = new Vector2(0,0);
	}

	private void OnTriggerStay2D(Collider2D other)
	{
		if (!isActiveAndEnabled)
		{
			return;
		}

		CloudProjectile _cloud = other.gameObject.GetComponent<CloudProjectile>();
		if (_cloud == null)
		{
			return;
		}
		
		Rigidbody2D _rb = _cloud.GetComponent<Rigidbody2D>();
		
		if (!isBeingBlown)
		{
			force = new Vector2(-fanForce * Random.Range(0.9f, 1.1f),Random.Range(-.5f * fanForce,.5f * fanForce));
			isBeingBlown = true;
		}
		_rb.AddForce(force);
		Debug.Log("force = "+force);
	
	
	}


// the following two methods have no references.  ARE THEY BEING USED?????
	private float GetDistanceToCenter(Vector2 cloudPosition)
	{
		
		float _dist = 0f;
		Transform fanTransform = gameObject.GetComponentInParent<Transform>();

		_dist = Vector2.Distance(cloudPosition, fanTransform.position);
		return _dist;
	}

	private Vector2 GetFanPower(float distance)
	{
		float colliderSize = GetComponent<BoxCollider2D>().size.x;
		float percent = (1 - (distance/colliderSize)) * fanForce;
		Vector2 power = new Vector2(1,0)*percent;
		return power;
	}

	private void OnTriggerExit2D(Collider2D other) {
		isBeingBlown = false;
	}
}
