﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Feedbacks;

public class Laser_Mulitiple : MonoBehaviour
{

    public float timeOn = 3f;
    public float timeOff = 1f;
    public float rotationSpeed = 2f;
    public LayerMask laserMask;
    public LayerMask killMask;
    public float numberOfLasers = 4;

    public LineRenderer laserLineRenderer;
    public float lineSegmentLength;
    public float minLineVariation = 0f;
    public float maxLineVariation = 0f;

    public MMFeedbacks contactFeedbacks;
    public MMFeedbacks startFeedbacks;
    public MMFeedbacks originFeedbacks;

    protected bool isFiring = false;
    protected bool canFire = true;
    protected bool isVisible = false;

    //need to revfactor laser to pass these values rather than having them global.
    protected Vector2 contactPoint;
    protected float rayLength;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
