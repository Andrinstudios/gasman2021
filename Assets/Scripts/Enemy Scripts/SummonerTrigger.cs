using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class SummonerTrigger : MonoBehaviour
{
    public List<GMCharacterSummon> summoners;
    public float delayBeforeFirstSummon = 3f;

    protected virtual void TriggerSummoners()
    {
        foreach (GMCharacterSummon summoner in summoners)
        {
            summoner.isTriggered = true;

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            StartCoroutine(StartTrigger());
        }
    }

    protected virtual IEnumerator StartTrigger()
    {
        yield return new WaitForSeconds(delayBeforeFirstSummon);
        TriggerSummoners();
    }

}
