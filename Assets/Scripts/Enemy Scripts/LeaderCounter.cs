﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class LeaderCounter : MonoBehaviour
{   [HideInInspector]
    public int numberOfEnemies;
    private int counter = 0;


    private void Awake()
    {
        //register with the sceneDataHolder
        SceneDataHolder _sceneData = FindObjectOfType<SceneDataHolder>();
        GMSaveLoad saveable = GetComponent<GMSaveLoad>();

        _sceneData.AddEnemyToSaveableList(saveable);

        GMProximityManager pManager = FindObjectOfType<GMProximityManager>();
        pManager.AddManagedObect(GetComponent<ProximityManaged>());
    }

    public void AdvanceKillCounter()
    {
        counter += 1;

        if (counter == numberOfEnemies)
        {
            this.gameObject.SetActive(false);
        }
    }
}
