﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potpurri : MonoBehaviour {

	public LayerMask _collisionMask;

	// Use this for initialization
	void Start () {
		
	}

	private void OnTriggerEnter2D(Collider2D other) {
		if (!isActiveAndEnabled)
		{
			return;
		}

		//check that object is in the mask
		int layer = other.gameObject.layer;
		if (_collisionMask != (_collisionMask | (1 << layer)))
		{
			return;
		}

		CloudProjectile _cloud = other.gameObject.GetComponent<CloudProjectile>();
		if (_cloud == null)
		{
			_cloud = other.gameObject.GetComponentInParent<CloudProjectile>();
		}
		Debug.Log("flower collision");
		_cloud.Potpurried();
	}
}
