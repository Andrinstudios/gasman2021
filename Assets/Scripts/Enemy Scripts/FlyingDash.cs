using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;

public class FlyingDash : CharacterDash
{
    public float recoveryHeight = 5f;
    public MMFeedbacks _predashFB;

    protected CharacterFly _fly;
    protected bool preDashing = false;
    protected MMPath path;
    protected bool hasPath = true;


    protected override void Initialization()
    {
        _fly = GetComponent<CharacterFly>();
        path = GetComponent<MMPath>();
        if(path != null)
        {
            if (path.PathElements == null || path.PathElements.Count < 1)
            {
                hasPath = false;
            }
        }
        else
        {
            hasPath = true;
        }

        base.Initialization();

    }

    public override void StartDash()
    {
        _fly.enabled = false;
        base.StartDash();
       
    }

    public override void StopDash()
    {
        _fly.enabled = true;
        preDashing = false;
        base.StopDash();
      //  Recovery();
      //  path.UpdateOriginalTransformPosition(this.gameObject.transform.position);

    }

    protected override void ComputeDashDirection()
    {

        Vector3 _playerPosition = LevelManager.Instance.Players[0].transform.position;
        _dashDirection = _playerPosition - this.transform.position;

        CheckAutoCorrectTrajectory();

        if (_dashDirection.magnitude < MinimumInputThreshold)
        {
            _dashDirection = _character.IsFacingRight ? Vector2.right : Vector2.left;
        }
        else
        {
            _dashDirection = _dashDirection.normalized;
        }
    }

    public virtual void Recovery()
    {
        _fly.SetVerticalMove(1f);

    }

    public virtual void PlayPredashFeedbacks()
    {
        if (!preDashing)
        {
           _predashFB?.PlayFeedbacks();
            preDashing = true;
        }
        
    }

    protected IEnumerator  ResetPath()
    {
        yield return new WaitForSeconds(2.5f);
        path.UpdateOriginalTransformPosition(this.gameObject.transform.position);
    }

}
