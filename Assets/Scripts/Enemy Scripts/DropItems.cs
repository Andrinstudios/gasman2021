﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;

public class DropItems : MonoBehaviour
{
    public List <GMPickableItem> pickableItems;
    
    [Range(0, 100)]
    public int dropPercent = 100;
    public MMFeedbacks dropFeedback;

    //Get a reference to health so we can registre for the OnDeath delagte
    protected Health _health;

    private void Start()
    {
        _health = GetComponent<Health>();
    }

    private void OnEnable()
    {
        if (_health == null)
        {
            _health = GetComponent<Health>();
        }

        if (_health != null)
        {
            _health.OnDeath += OnDeath;
        }
    }

    private void OnDisable()
    {
        _health.OnDeath -= OnDeath;
    }

    protected virtual void DropItem()
    {
        if(pickableItems.Count < 1) { return; }

        int maxRange = 100 / dropPercent;
        int dropProbability = Random.Range(1, maxRange);

        switch (dropProbability)
        {
            case 1:
                int randItem = Random.Range(0, pickableItems.Count - 1);
                Transform spot = this.transform;
                Vector3 itemPosition = new Vector2(transform.position.x, transform.position.y + 2);
                GMPickableItem spawnItem = pickableItems[randItem];
                Instantiate(spawnItem, itemPosition, transform.rotation);
                Rigidbody2D itemBody = spawnItem.GetComponent<Rigidbody2D>();
                itemBody.AddForce(new Vector2(0, 10), ForceMode2D.Force);
               // dropFeedback.PlayFeedbacks();

                break;

            default:
                break;

        }
    }

    protected virtual void OnDeath()
    {
        Debug.Log("dead, now drop it");
        DropItem();
    }
}
