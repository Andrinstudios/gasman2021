﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;


public class KillCloud : MonoBehaviour {

	private void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.tag != "Cloud")
		{
			return;
		}
		
		CloudProjectile _cloud = other.gameObject.GetComponent<CloudProjectile>();
		if (_cloud.inVacuum)
		{
			Health cHealth = _cloud.GetComponent<Health>();
			cHealth.Kill();
			
		}
	}
}
