using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;


//Class to control the resetting behaviors of the boss.

public class SummonBossController : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{
    public Animator _animator;
    protected AIBrain _brain;

    private void Awake()
    {
        _brain = GetComponent<AIBrain>();
    }

    private void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {

        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.PlayerDeath:

                _brain.BrainActive = false;
                _brain.ResetBrain();

                break;

            case CorgiEngineEventTypes.Respawn:

                ResetCharacter();
                break;

            default:
                break;
        }
    }

    protected virtual void ResetCharacter()
    {
        _animator.SetBool("Summon", false);
        GetComponent<Health>().ResetHealthToMaxHealth();
    }

}
