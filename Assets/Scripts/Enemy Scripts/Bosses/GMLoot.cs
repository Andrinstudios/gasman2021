using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;


public class GMLoot : MonoBehaviour
{
    public enum LootSpawnModes { Local, Preset}


    [Header("Loot items")]
    
    public List<GameObject> lootObjects;
    public LootSpawnModes SpawnMode;
    public List<Transform> spawnLocations;

    [Header("Conditions")]
    /// if this is true, loot will happen when this object dies
    [Tooltip("if this is true, loot will happen when this object dies")]
    public bool SpawnLootOnDeath = true;
    /// if this is true, loot will happen when this object takes damage
    [Tooltip("if this is true, loot will happen when this object takes damage")]
    public bool SpawnLootOnDamage = false;

    [Header("Spawn")]
    /// a delay (in seconds) to wait for before spawning loot
    [Tooltip("a delay (in seconds) to wait for before spawning loot")]
    public float Delay = 0f;
    /// the minimum and maximum quantity of objects to spawn 
    [Tooltip("the minimum and maximum quantity of objects to spawn")]
    [MMVector("Min", "Max")]
    public Vector2 Quantity = Vector2.one;
    /// the position, rotation and scale objects should spawn at
    [Tooltip("the position, rotation and scale objects should spawn at")]
    public MMSpawnAroundProperties SpawnProperties;
    /// The maximum quantity of objects that can be looted from this Loot object
    [Tooltip("The maximum quantity of objects that can be looted from this object")]
    public int MaximumQuantity = 100;
    /// The remaining quantity of objects that can be looted from this Loot object, displayed for debug purposes 
    [Tooltip("The remaining quantity of objects that can be looted from this Loot object, displayed for debug purposes")]
    [MMReadOnly]
    public int RemainingQuantity = 100;
    [Header("Collisions")]
    /// Whether or not spawned objects should try and avoid obstacles 
    [Tooltip("Whether or not spawned objects should try and avoid obstacles")]
    public bool AvoidObstacles = false;
    /// the layer mask containing layers the spawned objects shouldn't collide with 
    [Tooltip("the layer mask containing layers the spawned objects shouldn't collide with")]
    [MMCondition("AvoidObstacles", true)]
    public LayerMask AvoidObstaclesLayerMask = LayerManager.ObstaclesLayerMask;
    /// the radius around the object within which no obstacle should be found
    [Tooltip("the radius around the object within which no obstacle should be found")]
    [MMCondition("AvoidObstacles", true)]
    public float AvoidRadius = 0.25f;
    /// the amount of times the script should try finding another position for the loot if the last one was within an obstacle. More attempts : better results, higher cost
    [Tooltip("the amount of times the script should try finding another position for the loot if the last one was within an obstacle. More attempts : better results, higher cost")]
    [MMCondition("AvoidObstacles", true)]
    public int MaxAvoidAttempts = 5;

    [Header("Feedback")]
    /// A MMFeedbacks to play when spawning loot. Only one feedback will play. If you want one per item, it's best to place it on the item itself, and have it play when the object gets instantiated. 
    [Tooltip("A MMFeedbacks to play when spawning loot. Only one feedback will play. If you want one per item, it's best to place it on the item itself, and have it play when the object gets instantiated.")]
    public MMFeedbacks LootFeedback;
    [Header("Debug")]
    /// if this is true, gizmos will be drawn to show the shape within which loot will spawn
    [Tooltip("if this is true, gizmos will be drawn to show the shape within which loot will spawn")]
    public bool DrawGizmos = false;
    /// the amount of gizmos to draw
    [Tooltip("the amount of gizmos to draw")]
    public int GizmosQuantity = 1000;
    /// the color the gizmos should be drawn with
    [Tooltip("the color the gizmos should be drawn with")]
    public Color GizmosColor = MMColors.LightGray;
    /// the size at which to draw the gizmos
    [Tooltip("the size at which to draw the gizmos")]
    public float GimosSize = 1f;
    /// a debug button used to trigger a loot
    [Tooltip("a debug button used to trigger a loot")]
    [MMInspectorButton("SpawnLootDebug")]
    public bool SpawnLootButton;

    protected Health _health;
    protected GameObject _objectToSpawn;
    protected GameObject _spawnedObject;
    protected Vector3 _raycastOrigin;
    protected RaycastHit2D _raycastHit2D;
    protected Collider[] _overlapBox;


    /// <summary>
    /// On Awake we grab the health component if there's one, and initialize our loot table
    /// </summary>
    protected virtual void Awake()
    {
        _health = this.gameObject.GetComponent<Health>();
        ResetRemainingQuantity();

        if (SpawnMode == LootSpawnModes.Preset)
        {
            if (spawnLocations.Count > spawnLocations.Count)
            {
                Debug.Log("WARNING: there are not enough locaitons for the loot to spawn at.  Be sure the number of locations is equal to the number of loot items.");
            }
        }
    }

    /// <summary>
    /// Resets the remaining quantity to the maximum quantity
    /// </summary>
    public virtual void ResetRemainingQuantity()
    {
        RemainingQuantity = MaximumQuantity;
    }

    /// <summary>
    /// This method spawns the specified loot after applying a delay (if there's one)
    /// </summary>
    public virtual void SpawnLoot()
    {
        StartCoroutine(SpawnLootCo());
    }

    /// <summary>
    /// A debug method called by the inspector button
    /// </summary>
    protected virtual void SpawnLootDebug()
    {
        if (!Application.isPlaying)
        {
            Debug.LogWarning("This debug button is only meant to be used while in Play Mode.");
            return;
        }

        SpawnLoot();
    }


    protected virtual IEnumerator SpawnLootCo()
    {
        if (Delay > 0)
        {
            yield return MMCoroutine.WaitFor(Delay);
        }

        SpawnAllLoot();
        LootFeedback?.PlayFeedbacks();
    }


    public virtual void SpawnAllLoot()
    {
        foreach (GameObject newLoot in lootObjects)
        {
            _spawnedObject = Instantiate(newLoot);
          //  Debug.Log("spawning " + _spawnedObject.gameObject.name);
            if (AvoidObstacles)
            {
                bool placementOK = false;
                int amountOfAttempts = 0;
                while (!placementOK && (amountOfAttempts < MaxAvoidAttempts))
                {
                    MMSpawnAround.ApplySpawnAroundProperties(_spawnedObject, SpawnProperties, this.transform.position);

                    _raycastOrigin = _spawnedObject.transform.position;
                    _raycastHit2D = Physics2D.BoxCast(_raycastOrigin + Vector3.right * AvoidRadius, AvoidRadius * Vector2.one, 0f, Vector2.left, AvoidRadius, AvoidObstaclesLayerMask);
                    if (_raycastHit2D.collider == null)
                    {
                        placementOK = true;
                    }
                    else
                    {
                        amountOfAttempts++;
                    }
                }
            }
            else
            {
                MMSpawnAround.ApplySpawnAroundProperties(_spawnedObject, SpawnProperties, this.transform.position);
            }
            _spawnedObject.SendMessage("OnInstantiate", SendMessageOptions.DontRequireReceiver);
        }
    }

    public virtual void SpawnAtLocation()
    {
        int index = 0;
        foreach(GameObject lootObject in lootObjects)
        {
            Instantiate(lootObject, spawnLocations[index].transform.position, Quaternion.identity);
            index++;
        }
    }

    /// <summary>
    /// On hit, we spawn loot if needed
    /// </summary>
    protected virtual void OnHit()
    {
        if (!SpawnLootOnDamage)
        {
            return;
        }

        SpawnLoot();
    }

    /// <summary>
    /// On death, we spawn loot if needed
    /// </summary>
    protected virtual void OnDeath()
    {
        if (!SpawnLootOnDeath)
        {
            return;
        }

        switch (SpawnMode)
        {
            case LootSpawnModes.Local:
                SpawnAllLoot();
                break;
            case LootSpawnModes.Preset:
                SpawnAtLocation();
                break;
            default:
                break;
        }

    }

    /// <summary>
    /// OnEnable we start listening for death and hit if needed
    /// </summary>
    protected virtual void OnEnable()
    {
        if (_health != null)
        {
            _health.OnDeath += OnDeath;
            _health.OnHit += OnHit;
        }
    }

    /// <summary>
    /// OnDisable we stop listening for death and hit if needed
    /// </summary>
    protected virtual void OnDisable()
    {
        if (_health != null)
        {
            _health.OnDeath -= OnDeath;
            _health.OnHit -= OnHit;
        }
    }

    /// <summary>
    /// OnDrawGizmos, we display the shape at which objects will spawn when looted
    /// </summary>
    protected virtual void OnDrawGizmos()
    {
        if (DrawGizmos)
        {
            MMSpawnAround.DrawGizmos(SpawnProperties, this.transform.position, GizmosQuantity, GimosSize, GizmosColor);
        }
    }
}
