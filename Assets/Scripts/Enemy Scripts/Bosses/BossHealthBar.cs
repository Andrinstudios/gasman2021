using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class BossHealthBar : MMProgressBar, MMEventListener<MMDamageTakenEvent>, MMEventListener<CorgiEngineEvent>
{
    [MMInspectorGroup("Custom",true,5)]
    public Character bossCharacter;


    protected override void OnEnable()
    {
        this.MMEventStartListening<MMDamageTakenEvent>();
        this.MMEventStartListening<CorgiEngineEvent>();

        base.OnEnable();
    }

    protected virtual void OnDisable()
    {
        this.MMEventStopListening<MMDamageTakenEvent>();
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    public virtual void OnMMEvent(MMDamageTakenEvent dEvent)
    {
        if (dEvent.AffectedCharacter == bossCharacter)
        {
            //get a reference to the characters health to set the maxHealth.
            Health _health = dEvent.AffectedCharacter.GetComponent<Health>();
            float _maxHealth = _health.MaximumHealth;

            UpdateBar(dEvent.CurrentHealth, 0,_maxHealth);
        }
    }

    public virtual void OnMMEvent(CorgiEngineEvent cEvent)
    {
        if (cEvent.EventType == CorgiEngineEventTypes.Respawn)
        {
            SetBar01(1);
            this.gameObject.SetActive(false);
        }
    }

}
