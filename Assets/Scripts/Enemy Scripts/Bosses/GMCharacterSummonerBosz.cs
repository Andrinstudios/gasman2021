using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class GMCharacterSummonerBosz : GMCharacterSummon
{
    public List<Transform> spikeLocations;
    public List<Transform> spawnLocations;
    public Character spawningEnemy;

    public float gasSummonThreshold = 50f;
    public float timeBetweenSpawns = 30;

    
    bool canSummon = true;
    bool canSpawn = true;

    protected const string _summoningAnimationParameterName = "Summoning";
    protected int _summoningAnimationParameter;

    protected override void Initialization()
    {
        base.Initialization();
    }


    public override void Summon(Transform playerTransform)
    {
        if (!AbilityPermitted)
        {
            return;
        }

        if (triggerActivated && !isTriggered)
        {
            return;
        }

        _playerTransform = playerTransform;

        if (GasManager.Instance.currentGasLevel < 75 && canSpawn == true)
        {
            SpawnEnemy();
        }

        if (!canSummon)
        {
            return;
        }

        _animator.SetBool("Summon", true);

        //choose a random attack type
        int attackType = Random.Range(1,4);
        Debug.Log("summon " + attackType);
        if (attackType == 1)
        {
            StartCoroutine(RandomFallingAttack());
            return;
        }

        if (attackType == 2)
        {
            StartCoroutine(SequncedFallingAttack());
            return;
        }

        if (attackType == 3)
        {
            StartCoroutine(GroundAttack());
            return;
        }

    }

    protected IEnumerator RandomFallingAttack()
    {
        //select random locations
        Vector3 _spikeLocation = new Vector3(0, 0, 0);
        for (int i = 0; i<=20; i++)
        {
            int index = Random.Range(0, spikeLocations.Count - 1);
            Transform _transform = spikeLocations[index];
            _spikeLocation = _transform.position;
            Instantiate(fallingAttack, _spikeLocation, Quaternion.identity);
            yield return new WaitForSeconds(0.15f);
        }
        _animator.SetBool("Summon", false);
    }

    public void CutSceneAttack()
    {
        foreach (Transform _transform in spikeLocations)
        {
            Instantiate(fallingAttack, _transform.position, Quaternion.identity);
            
        }
        
    }

    protected IEnumerator SequncedFallingAttack()
    {
        //Right to left
        foreach(Transform _transform in spikeLocations)
        {
            Instantiate(fallingAttack, _transform.position, Quaternion.identity);
            yield return new WaitForSeconds(0.1f);
        }

        //left to right
        for(int i = spikeLocations.Count-2; i >= 0; i--)
        {
            Transform _transform = spikeLocations[i];
            //shift the spikes a small amount
            Vector3 shiftedPosition = new Vector3(_transform.position.x - 1f, _transform.position.y, _transform.position.z);

            Instantiate(fallingAttack, shiftedPosition, Quaternion.identity);
            yield return new WaitForSeconds(0.1f);
        }
        _animator.SetBool("Summon", false);
    }

    protected IEnumerator GroundAttack()
    {
        for (int i = 0; i <=6; i++)
        {
            SummonBelow();
            yield return new WaitForSeconds(0.75f);
        }
        _animator.SetBool("Summon", false);
    }


    protected void SpawnEnemy()
    {
        int numberToSpawn = Random.Range(1, 4);

        int i = 0;
        List<int> wValues = new List<int>();
        while (i <= numberToSpawn-1)
        {
            int index = Random.Range(0, spawnLocations.Count - 1);

            //check to see if this location is already selected and add if it is not.
            if (!wValues.Contains(index))
            {
                wValues.Add(index);
                Transform _transform = spawnLocations[index];
                Instantiate(spawningEnemy.gameObject, _transform.position, Quaternion.identity);
                i++;
            }
        }

        _animator.SetBool("Summon", false);
        StartCoroutine(SpawnTimer());
    }

    protected IEnumerator SpawnTimer()
    {
        canSpawn = false;
        yield return new WaitForSeconds(timeBetweenSpawns);
        canSpawn = true;
    }

    //Animation

    /// <summary>
    /// Adds required animator parameters to the animator parameters list if they exist
    /// </summary>
    protected override void InitializeAnimatorParameters()
    {

        RegisterAnimatorParameter(_summoningAnimationParameterName, AnimatorControllerParameterType.Bool, out _summoningAnimationParameter);
    }

    /// <summary>
    /// Sends the current speed and the current value of the Walking state to the animator
    /// </summary>
    public override void UpdateAnimator()
    {
        MMAnimatorExtensions.UpdateAnimatorBool(_animator, _summoningAnimationParameter, true, _character._animatorParameters);
    }


    //receives the GMEnemyTeleport.Teleport delegate 
    protected override void OnTeleport()
    {
        canSummon = false;
        StartCoroutine(InterruptSummon(8));
        base.OnTeleport();
    }

    //recevies the Health.OnHit delegate
    protected override void OnHit()
    {
        StartCoroutine(InterruptSummon(2));
    }

    protected IEnumerator InterruptSummon(float delay)
    {
        canSummon = false;
        _animator.SetBool("Summon", false);
        yield return new WaitForSeconds(delay);
        canSummon = true;
    }

    protected override void Reset()
    {
        canSpawn = true;
        canSummon = true;
        base.Reset();
    }

    public override void ResetAbility()
    {
        Debug.Log("resetting ability");
        base.ResetAbility();
    }

    public override void OnMMEvent(CorgiEngineEvent thisEvent)
    {
        if (thisEvent.EventType == CorgiEngineEventTypes.PlayerDeath)
        {
            StartCoroutine(CancelSummonsTimer());
        }

        base.OnMMEvent(thisEvent);
    }

    protected IEnumerator CancelSummonsTimer()
    {
        yield return new WaitForSeconds(0.5f);
        CancelSummons();
    }

    protected virtual void CancelSummons()
    {
        StopAllCoroutines();
    }
}
