using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class DeathFader : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{
    public MMFader fader;
    public float IntroFadeDuration = 2;


    protected virtual void FadeOut()
    {
        MMFadeInEvent.Trigger(IntroFadeDuration, fader.DefaultTween, fader.ID, false, Vector3.zero);
    }

    protected virtual void FadeIn()
    {
        MMFadeOutEvent.Trigger(IntroFadeDuration, fader.DefaultTween, fader.ID, false, Vector3.zero);
    }



    private void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {

        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.PlayerDeath:

                FadeOut();

                break;
            case CorgiEngineEventTypes.Respawn:

                FadeIn();

                break;
            default:
                break;
        }
    }

}
