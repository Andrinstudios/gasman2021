using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineTrigger : MonoBehaviour
{
    private PlayableDirector director;
    private bool isTriggered = false;



    private void Awake()
    {
        director = GetComponent<PlayableDirector>();
        director.played += Director_Played;
        director.stopped += Director_Stopped;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && !isTriggered)
        {
            StartTimeline();
        }
    }


    private void Director_Played(PlayableDirector obj)
    {
        Debug.Log("timeline played");
    }

    private void Director_Stopped(PlayableDirector obj)
    {
        Debug.Log("timeline stopped");
    }

    public void StartTimeline()
    {
        director.Play();
    }
}
