﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ParitilceAttTest : MonoBehaviour {

    public Transform attractorTransform;
    [HideInInspector]
    public List<ParticleCollisionEvent> colEvents;

    private void Start()
    {
        colEvents = new List<ParticleCollisionEvent>();
    }

    private void OnParticleTrigger()
    {
        ParticleSystem particles = GetComponent<ParticleSystem>();

        List<ParticleSystem.Particle> enter = new List<ParticleSystem.Particle>();

        int entered = particles.GetTriggerParticles(ParticleSystemTriggerEventType.Enter, enter);


        for (int i = 0; i < entered; i++)
        {
            ParticleSystem.Particle p = enter[i];

            Vector3 direction = attractorTransform.position - p.position;

            p.velocity = direction * 3;
            p.startColor = new Color32(255, 0, 0, 255);


            enter[i] = p;
        }

        particles.SetTriggerParticles(ParticleSystemTriggerEventType.Enter, enter);
    }

    private void OnParticleCollision(GameObject other)
    {
        ParticleSystem particles = GetComponent<ParticleSystem>();
        int colNumber = particles.GetCollisionEvents(other, colEvents);

        for (int i = 0; i < colNumber; i++)
        {
            

        }
    }
}
