﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MoreMountains.CorgiEngine;

public class SavePosition : MonoBehaviour
{
    // Generate a unique ID for this GameObject.
    public string ID;

    void Awake()
    {
        ID = this.gameObject.scene.name + this.gameObject.name;
        transform.localPosition = ES3.Load<Vector3>(ID, transform.localPosition);
        Debug.Log("Loading position: " + transform.localPosition);
        LoadHealth();
    }

    void OnDestroy()
    {
        if (this.gameObject != null)
        {
            ES3.Save<Vector3>(ID, transform.localPosition);
            Debug.Log("Saving position: " + transform.localPosition);
            SaveHealth();
        }
    }

    void SaveHealth()
    {
        string healthID = ID+"health";
        Health enemyHealth = gameObject.GetComponent<Health>();
        ES3.Save<int>(ID+"health", enemyHealth.CurrentHealth);

    }

    void LoadHealth()
    {
        string healthID = ID + "health";
        Health enemyHealth = gameObject.GetComponent<Health>();
        enemyHealth.InitialHealth = ES3.Load<int>(ID + "health", enemyHealth.CurrentHealth);
    }
}
