﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM_LevelBackground : MonoBehaviour {

/// If true, the background will stick to the camera
		public bool FollowCamera=true;
		/// If true, the background will stick to the camera even in edit mode
		public bool FollowCameraEditMode=true;

	    protected Camera _cameraController ;
		
		/// <summary>
		/// On enable, we get the main camera
		/// </summary>
		protected virtual void OnEnable () 
		{		
			// we get the camera				
			_cameraController = FindObjectOfType<Camera>();			
		}

	    /// <summary>
	    /// Every update, we make the level follow the camera's position
	    /// </summary>
	    protected  virtual void LateUpdate()
		{
			if (_cameraController==null)
				return;
				
			if (FollowCamera)
			{
				// if we're in editor mode and if the background is not supposed to follow the camera in edit mode, we exit
				if ( (!FollowCameraEditMode) && (Application.isEditor) )
					return;
					
				// we set the actual transform's position
				transform.position=new Vector3(_cameraController.transform.position.x,_cameraController.transform.position.y,transform.position.z);
			}
		}
}
