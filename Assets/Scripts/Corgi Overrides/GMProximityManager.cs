﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class GMProximityManager : ProximityManager, MMEventListener<CorgiEngineEvent>
{
    public override void OnMMEvent(CorgiEngineEvent engineEvent)
    {
        base.OnMMEvent(engineEvent);

        switch (engineEvent.EventType)
        {
            case CorgiEngineEventTypes.PlayerDeath:
                break;
            case CorgiEngineEventTypes.Respawn:
                break;
            default:
                break;
        }
    }

    public virtual void AddManagedObect(ProximityManaged managedObject)
    {
        ControlledObjects.Add(managedObject);
    }

    public virtual void RemoveManagedObject(ProximityManaged managedObject)
    {
        ControlledObjects.Remove(managedObject);
    }
}
