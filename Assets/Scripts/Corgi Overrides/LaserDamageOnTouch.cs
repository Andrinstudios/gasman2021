﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class LaserDamageOnTouch : DamageOnTouch
{
    public void HitByRay(Collider2D _col)
    {
        Colliding(_col);
    }

    public void StayInRay(Collider2D _col)
    {
        Colliding(_col);
    }

    public void ExitRay(Collider2D _col)
    {

    }

    protected override void Colliding(Collider2D collider)
    {
        base.Colliding(collider);
    }

}
