﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class GMInputManager : InputManager
{
    public MMInput.IMButton ModifyFireButton { get; protected set; }

    protected override void InitializeButtons()
    {
        base.InitializeButtons();
        ButtonList.Add(ModifyFireButton = new MMInput.IMButton(PlayerID, "ModifyFire", ModifyFireButtonDown, ModifyFireButtonPressed, ModifyFireButtonUp));
    }


    public virtual void ModifyFireButtonDown() { ModifyFireButton.State.ChangeState((MMInput.ButtonStates.ButtonDown)); }
    public virtual void ModifyFireButtonPressed() { ModifyFireButton.State.ChangeState(MMInput.ButtonStates.ButtonPressed); }
    public virtual void ModifyFireButtonUp() { ModifyFireButton.State.ChangeState(MMInput.ButtonStates.ButtonUp); }
    

}
