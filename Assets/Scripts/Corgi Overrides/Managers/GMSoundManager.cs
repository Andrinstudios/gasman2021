﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class GMSoundManager : MMPersistentSingleton<GMSoundManager>{


		public virtual AudioSource PlaySound(AudioClip sfx, Vector3 location, bool loop=false)
		{
			if (!SoundManager.Instance.Settings.SfxOn)
				return null;
			
			// we create a temporary game object to host our audio source
			GameObject temporaryAudioHost = new GameObject("TempAudio");
			// we set the temp audio's position
			temporaryAudioHost.transform.position = location;
			// we add an audio source to that host
			AudioSource audioSource = temporaryAudioHost.AddComponent<AudioSource>() as AudioSource; 
			// we set that audio source clip to the one in paramaters
			audioSource.clip = sfx; 
			// we set the audio source volume to the one in parameters
			audioSource.volume = SoundManager.Instance.SfxVolume;
			// we set our loop setting
			audioSource.loop = loop;
			//add random pitch variation to the sound
			float newPitch = Random.Range(0.75f, 1.25f);
			audioSource.pitch = newPitch;

			// we start playing the sound
			audioSource.Play(); 

			if (!loop)
			{
				// we destroy the host after the clip has played
				Destroy(temporaryAudioHost, sfx.length);
			}

			// we return the audiosource reference
			return audioSource;
		}
}



	

