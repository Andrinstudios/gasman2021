﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using MoreMountains.Tools;

public class GMInputSystemManager : InputManager
{
    protected bool _inputActionsEnabled = true;
    protected bool _initialized = false;

    public GasManInputActions1 InputActions;

    public MMInput.IMButton ModifyFireButton { get; protected set; }

    protected override void Start()
    {
        if (!_initialized)
        {
            Initialization();
        }
    }

    protected override void InitializeButtons()
    {
        base.InitializeButtons();
        ButtonList.Add(ModifyFireButton = new MMInput.IMButton(PlayerID, "ModifyFire", ModifyFireButtonDown, ModifyFireButtonPressed, ModifyFireButtonUp));
        Debug.Log("added modify");
    }


    public virtual void ModifyFireButtonDown() { ModifyFireButton.State.ChangeState((MMInput.ButtonStates.ButtonDown)); }
    public virtual void ModifyFireButtonPressed() { ModifyFireButton.State.ChangeState(MMInput.ButtonStates.ButtonPressed); }
    public virtual void ModifyFireButtonUp() { ModifyFireButton.State.ChangeState(MMInput.ButtonStates.ButtonUp); }




    protected override void Initialization()
    {
        base.Initialization();

        _inputActionsEnabled = true;

        InputActions = new GasManInputActions1();

        InputActions.PlayerControls.PrimaryMovement.performed += context => _primaryMovement = context.ReadValue<Vector2>();
        InputActions.PlayerControls.SecondaryMovement.performed += context => _secondaryMovement = context.ReadValue<Vector2>();

        InputActions.PlayerControls.Jump.performed += context => { BindButton(context, JumpButton); };
     //   InputActions.PlayerControls.Run.performed += context => { BindButton(context, RunButton); };
        InputActions.PlayerControls.Dash.performed += context => { BindButton(context, DashButton); };
        InputActions.PlayerControls.Shoot.performed += context => { BindButton(context, ShootButton); };
        InputActions.PlayerControls.SecondaryShoot.performed += context => { BindButton(context, SecondaryShootButton); };
        InputActions.PlayerControls.Interact.performed += context => { BindButton(context, InteractButton); };
     //   InputActions.PlayerControls.Reload.performed += context => { BindButton(context, ReloadButton); };
        InputActions.PlayerControls.Pause.performed += context => { BindButton(context, PauseButton); };
     //   InputActions.PlayerControls.SwitchWeapon.performed += context => { BindButton(context, SwitchWeaponButton); };
     //   InputActions.PlayerControls.SwitchCharacter.performed += context => { BindButton(context, SwitchCharacterButton); };
     //   InputActions.PlayerControls.TimeControl.performed += context => { BindButton(context, TimeControlButton); };

     //   InputActions.PlayerControls.Swim.performed += context => { BindButton(context, SwimButton); };
     //   InputActions.PlayerControls.Glide.performed += context => { BindButton(context, GlideButton); };
     //   InputActions.PlayerControls.Jetpack.performed += context => { BindButton(context, JetpackButton); };
     //   InputActions.PlayerControls.Fly.performed += context => { BindButton(context, FlyButton); };
        InputActions.PlayerControls.Grab.performed += context => { BindButton(context, GrabButton); };
        InputActions.PlayerControls.Throw.performed += context => { BindButton(context, ThrowButton); };
     //   InputActions.PlayerControls.Push.performed += context => { BindButton(context, PushButton); };
        InputActions.PlayerControls.ModifyFire.performed += context => { BindButton(context, ModifyFireButton); };

        _initialized = true;


    }

    /// <summary>
    /// Changes the state of our button based on the input value
    /// </summary>
    /// <param name="context"></param>
    /// <param name="imButton"></param>
    protected virtual void BindButton(InputAction.CallbackContext context, MMInput.IMButton imButton)
    {
        var control = context.control;

        if (control is ButtonControl button)
        {
            if (button.wasPressedThisFrame)
            {
                imButton.State.ChangeState(MMInput.ButtonStates.ButtonDown);
            }
            if (button.wasReleasedThisFrame)
            {
                imButton.State.ChangeState(MMInput.ButtonStates.ButtonUp);
            }
        }
    }

    protected override void Update()
    {
        if (IsMobile && _inputActionsEnabled)
        {
            _inputActionsEnabled = false;
            InputActions.Disable();
        }

        if (!IsMobile && (InputDetectionActive != _inputActionsEnabled))
        {
            if (InputDetectionActive)
            {
                _inputActionsEnabled = true;
                InputActions.Enable();
            }
            else
            {
                _inputActionsEnabled = false;
                InputActions.Disable();
            }
        }
    }

    /// <summary>
    /// On enable we enable our input actions
    /// </summary>
    protected virtual void OnEnable()
    {
        if (!_initialized)
        {
            Initialization();
        }
        InputActions.Enable();
    }

    /// <summary>
    /// On disable we disable our input actions
    /// </summary>
    protected virtual void OnDisable()
    {
        InputActions.Disable();
    }
}

