﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

public class DoorTEST : MonoBehaviour
{
    public AudioClip clip;


    public void PlayClip()
    {
        SoundManager.Instance.PlaySound(clip, transform.position, false);
        BoxCollider2D doorCollider = GetComponent<BoxCollider2D>();
        doorCollider.enabled = false;
        Debug.Log("door ulnlocked");
    }
}
