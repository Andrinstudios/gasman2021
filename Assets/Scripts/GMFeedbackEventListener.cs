﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;

public class GMFeedbackEventListener : MonoBehaviour, MMEventListener<GasIncreaseEvent>
{

    public MMFeedbacks gasFeedbacks;
        private void OnEnable() 
    {
        this.MMEventStartListening<GasIncreaseEvent>();    
    }

    private void OnDisable() 
    {
        this.MMEventStopListening<GasIncreaseEvent>();    
    }

    public virtual void OnMMEvent(GasIncreaseEvent gasEvent)
    {
        Debug.Log("you picked up" + gasEvent);
        PlayGaSFeedbacks();
    }

    protected virtual void PlayGaSFeedbacks()
		{
            gasFeedbacks.PlayFeedbacks(this.transform.position);
        }	
}
