﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;


/* class for managing the gas amounts for the player
 * will hold total amount and increment and decrement as needed.
 */

public class GasManager : MMPersistentSingleton<GasManager>
{
    private Character _character;
    public Text GasLabelText;
    public float baseMaxGasLevel = 100f;
    private float maxGasLevel;
    private int gasLevelLabelCounter;
    public float startingGasLevel = 100f;
    public float baseRegenRate = 1;
    private float regenRate;
    public float regenDelay = 5f;
    protected WaitForSeconds regenWFS;

    private bool isReplenishingGas = false;

    [Header("Feedback")]
    public MMFeedbacks fuelAddedFeedback;
    public MMFeedbacks feulRemovedFeedback;
    public MMFeedbacks fuelFullFeedback;
    public MMFeedbacks fuelEmptyFeedback;
    public MMFeedbacks tankIncreaseFeedback;

    [HideInInspector]
    public float currentGasLevel;
    [HideInInspector]
    public bool isFull = true;
    private IEnumerator replaceGasCoroutine;


    // Use this for initialization
    void Start()
    {
        _character = LevelManager.Instance.Players[0];
        startingGasLevel = baseMaxGasLevel;
        currentGasLevel = startingGasLevel;

        maxGasLevel = baseMaxGasLevel;
        regenRate = baseRegenRate;
        regenWFS = new WaitForSeconds(regenDelay);

        replaceGasCoroutine = ReplenishGas();
    }


    /// <summary>
    /// Adds gas to the tank by fixed amount
    /// </summary>
    public void AddGas(float gasAmount)
    {
        if (currentGasLevel >= maxGasLevel) { return; }

        gasLevelLabelCounter = (int)currentGasLevel;
        currentGasLevel += gasAmount;

        if (currentGasLevel >= maxGasLevel)
        {
            currentGasLevel = maxGasLevel;
            fuelFullFeedback?.PlayFeedbacks();
        }
        else
        {
            fuelAddedFeedback?.PlayFeedbacks();
        }

        if(currentGasLevel >= maxGasLevel)
        {
            isFull = true;
        }

        UpdateGasBar();
    }

    /// <summary>
    /// Removes gas from tank by fixed amount.
    /// </summary>
    public void RemoveGas(float gasAmount)
    {
        if (currentGasLevel <= 0) { return; }
        isReplenishingGas = false;
        StopCoroutine(replaceGasCoroutine);
        replaceGasCoroutine = ReplenishGas();
        
        gasLevelLabelCounter = (int)currentGasLevel;
        currentGasLevel -= gasAmount;
        if (currentGasLevel < 0) 
        { 
            currentGasLevel = 0;
        }

        isFull = false;
        UpdateGasBar();

        StartCoroutine(replaceGasCoroutine);
    }

    // set up for constant replenishment of gas to the tank. 
    public IEnumerator ReplenishGas()
    {
        //replenishment has a delay after using before starting up again.
        yield return regenWFS;
        isReplenishingGas = true;
        float currentTime = Time.time;
        while(currentGasLevel <= maxGasLevel)
        {
            if (Time.time > currentTime)   // sets a timer to update the level once per second.
            {
                gasLevelLabelCounter = (int)currentGasLevel;
                currentGasLevel += regenRate;

                if (currentGasLevel >= maxGasLevel)
                {
                    isFull = true;
                }

                UpdateGasBar();
                currentTime = Time.time + 1f;
            }
            yield return 0;
        }
        isReplenishingGas = false;
    }

    /// <summary>
    ///  Increases the max gas level for the character by a fixed amount
    /// </summary>
    public void ChangeMaxGasLevel(float maxChange)
    {
        gasLevelLabelCounter = (int)currentGasLevel;
        maxGasLevel += maxChange;
        baseMaxGasLevel = maxGasLevel;
        currentGasLevel = maxGasLevel;

        if (currentGasLevel >= maxGasLevel)
        {
            isFull = true;
        }

        // tankIncreaseFeedback.PlayFeedbacks();

        //this needs to change the EndValue of the bar as well.

        UpdateGasBar();
    }

    /// <summary>
    /// Changes the gas regen rate by a percentage.
    /// </summary>
    public void ChangeGasRegenRate(float percentIncrease)
    {
        regenRate = regenRate * (1 + percentIncrease/100);
        baseRegenRate = regenRate;
        UpdateGasBar();
    }

    private void UpdateGasBar()
    {
        // Debug.Log("update "+gasLevelLabelCounter+ " "+currentGasLevel);
        if (GUIManager.Instance != null)     // && (_character.CharacterType == Character.CharacterTypes.Player)
        {
            GUIManager.Instance.UpdateJetpackBar(currentGasLevel, 0f, maxGasLevel, _character.PlayerID);
        }
        
        if(isReplenishingGas)
        {
            GasLabelText.text = currentGasLevel.ToString("000");
            return;
        }
        
            StopCoroutine(LerpLabel(0));
            //  GasLabelText.text = tempGasLabelValue.ToString("000");
            StartCoroutine(LerpLabel((int)currentGasLevel));
        
    }

    public void ResetToMaxGasLevel()
    {
        currentGasLevel = maxGasLevel;

        if (currentGasLevel >= maxGasLevel)
        {
            isFull = true;
        }

        UpdateGasBar();
    }


    IEnumerator LerpLabel(int target)
    {
        int start = gasLevelLabelCounter;
        for (float timer = 0f; timer < 0.5f; timer += Time.deltaTime )
        {
            float progress = timer / 0.5f;
            GasLabelText.text = Mathf.Lerp(start, target, progress).ToString("000");
            yield return null;
        }
        GasLabelText.text = target.ToString("000");
    }

    public void StopRechargingGas()
    {
        StopCoroutine(ReplenishGas());
    }

    public void PlayEmptySFX()
    {
        fuelEmptyFeedback.PlayFeedbacks();
    }
}

