﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;

public class GMCharacterDropItem : MonoBehaviour
{
    public int dropPercentage = 50;
    public bool isKeyCarrier = false;
    public List<GameObject> keys;
    public List<GameObject> items;
    public List<GameObject> weapons;
    public float ejectionForceX = 20f;
    public float ejectionForceY = 10f;

    public MMFeedbacks pickedFeedbacks;

    protected Health _health;

    bool hasInventory = false;

    protected void Start() 
    {
        if (keys.Count > 0 || items.Count > 0  || weapons.Count > 0 )
        {
            hasInventory = true;
        }

        _health = this.gameObject.GetComponentInParent<Health>();

    }

    protected virtual void OnDeath()
    {
        Debug.Log("OnDeath");
        if (hasInventory)
        {
            Debug.Log("startDrop");
            StartCoroutine(StartItemDrop());
        }

    }

    private void PlayFeedbacks()
    {
        pickedFeedbacks?.PlayFeedbacks();
    }


    IEnumerator StartItemDrop()
    {
        // set rhe randon direti of the spawn
        float xDirection = Random.Range(-1, 1);

        if (xDirection < 1)
        {
            ejectionForceX = -ejectionForceX;
        }


        if (isKeyCarrier)
        {
            foreach (GameObject key in keys)
            {
                Transform spot = this.transform;
                Vector3 keyPosition = new Vector2(transform.position.x, transform.position.y +2);
                spot.position = keyPosition;
                PlayFeedbacks();
                Instantiate(key, spot.position, spot.rotation);
                Rigidbody2D keyBody = key.GetComponent<Rigidbody2D>();
                keyBody.AddForce(new Vector2 (ejectionForceX ,ejectionForceY));
            }

            yield return new WaitForSeconds(0.25f);
    
        }

        // enemy is not carrying a key so check for other items
        if (items.Count > 0 || weapons.Count > 0)
        {
            int maxRange = 100/dropPercentage * 2;
            int probX = Random.Range(1, maxRange);
  
            switch(probX)
            {
                case 1:
                    int randItem = Random.Range(0, items.Count - 1);
                    Transform spot = this.transform;
                    Vector3 itemPosition = new Vector2(transform.position.x, transform.position.y +2);
                    PlayFeedbacks();
                    GameObject spawnItem = items[randItem];
                    Instantiate(spawnItem, itemPosition, transform.rotation);
                    Rigidbody2D itemBody = spawnItem.GetComponent<Rigidbody2D>();
                    itemBody.AddForce(new Vector2(ejectionForceX, ejectionForceY), ForceMode2D.Force);
                  //  AbilityStartFeedbacks.PlayFeedbacks();

                    Debug.Log("eject " + itemBody.name);

                    break;

                case 2:
                    int randWeapon = Random.Range(0, weapons.Count - 1);
                    Transform wSpot = this.transform;
                    Vector3 weaponPosition = new Vector2(transform.position.x, transform.position.y +2);
                    PlayFeedbacks();
                    GameObject spawnWeapon = items[randWeapon];
                    Instantiate(spawnWeapon, weaponPosition, transform.rotation);
                    Rigidbody2D weaponBody = spawnWeapon.GetComponent<Rigidbody2D>();
                    weaponBody.AddForce(new Vector2(ejectionForceX, ejectionForceY));

                    break;
                default:
                    Debug.Log("no drop");

                    break;
            }

                yield return new WaitForSeconds(0.25f);
       
        }


    }

    private void OnEnable()
    {
        if (_health == null)
        {
            _health = GetComponent<Health>();
        }

        if (_health != null)
        {
            _health.OnDeath += OnDeath;
        }
    }

    private void OnDisable()
    {
        _health.OnDeath -= OnDeath;
    }

}
