﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class GMCharacterLaunch : CharacterAbility
{
    public float launchForce = 10f;
    public bool isOnLaunchPad = false;
    public float gasPerLaunch = 10;

      protected int _launchingAnimationParameter;
      protected const string _launchingAnimationParameterName = "Launching";


    protected override void HandleInput()
    {
        if ((_inputManager.ShootButton.State.CurrentState == MMInput.ButtonStates.ButtonDown)
			&& (_verticalInput < -_inputManager.Threshold.y) && isOnLaunchPad)
		{

            if (gasPerLaunch > GasManager.Instance.currentGasLevel)
            {
                // Play empty sound effect
                GasManager.Instance.PlayEmptySFX();
                return;
            }
            Launch();
		}
    }

    private void Launch()
    {
        GasManager.Instance.RemoveGas(gasPerLaunch);
        _controller.AddVerticalForce(launchForce);
    }

       private void OnTriggerEnter2D(Collider2D other)
   {
       if (other.tag == "Launchpad")
       {
           isOnLaunchPad = true;
       }
   }

   private void OnTriggerExit2D(Collider2D other)
   {
       if (other.tag == "Launchpad")
       {
           isOnLaunchPad = false;
       }
   }

   /// <summary>
		/// Adds required animator parameters to the animator parameters list if they exist
		/// </summary>
		protected override void InitializeAnimatorParameters()
		{
			RegisterAnimatorParameter(_launchingAnimationParameterName, AnimatorControllerParameterType.Bool, out _launchingAnimationParameter);
		}

		/// <summary>
		/// At the end of the cycle, we update our animator's Dashing state 
		/// </summary>
		public override void UpdateAnimator()
		{
			MMAnimatorExtensions.UpdateAnimatorBool(_animator,-_launchingAnimationParameter,(_movement.CurrentState == CharacterStates.MovementStates.Dashing), _character._animatorParameters);
		}


}
