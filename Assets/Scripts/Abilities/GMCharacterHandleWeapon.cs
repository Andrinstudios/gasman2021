﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class GMCharacterHandleWeapon : CharacterHandleWeapon
{
   // protected GMInputManager _gInputManager;
    protected GMInputSystemManager _gInputManager;

    protected override void Initialization()
    {
        base.Initialization();

       // _gInputManager = GMInputManager.Instance as GMInputManager;
        _gInputManager = GMInputSystemManager.Instance as GMInputSystemManager;
    }

    protected override void HandleInput()
    {
        //check for atomic in process and if crouching for dive
        if (_gInputManager.ModifyFireButton.State.CurrentState == MMInput.ButtonStates.ButtonPressed || _movement.CurrentState == CharacterStates.MovementStates.Crouching)
		{
			return;
		}

        //check for dive initiation while in the air
        if(_inputManager.ShootButton.State.CurrentState == MMInput.ButtonStates.ButtonDown
				&& _verticalInput < -_inputManager.Threshold.y)
                {
                    return;
                }

        base.HandleInput();
    }

    public override void ShootStart()
    {
        // Don't allow attacks when pushing or crouching
        if ((_movement.CurrentState == CharacterStates.MovementStates.Crouching)
        ||  (_movement.CurrentState == CharacterStates.MovementStates.Pushing))
        {
            return;
        }
        base.ShootStart();
    }

    protected override void OnDeath()
    {
        base.OnDeath();
        GasManager.Instance.ResetToMaxGasLevel();
    }

}

