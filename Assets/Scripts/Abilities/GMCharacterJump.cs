﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;


/* This is a overidden class for CharacterJump to handle tying in teh GasManager
 * with the Jump class.  This iwll remove gas and play different feedbacks
 * for single jump and double jump
 */

public class GMCharacterJump : CharacterJump
{
    [Header("Gas Settings")]
    public bool firstJumpUsesGas = false;
    public float gasPerJump = 1;
    public float gasPerDoubleJump = 3;
    public MMFeedbacks doubleJumpFeedbacks;
    public MMFeedbacks expelGasFeedbacks;
    

    

    public override void JumpStart()
    {
       // bool onAOneWayPlatform = (_controller.OneWayPlatformMask.MMContains(_controller.StandingOn.layer)
       //         || _controller.MovingOneWayPlatformMask.MMContains(_controller.StandingOn.layer));
        if(EvaluateJumpConditions())
        {
            if (firstJumpUsesGas)
            {
                //check gas level for first jump and abort if low
                if (gasPerJump > GasManager.Instance.currentGasLevel)
                {
                    GasManager.Instance.PlayEmptySFX();
                    return;
                }
            }
            

            //this is the first jump
            if (NumberOfJumpsLeft == NumberOfJumps)
            {
                if(firstJumpUsesGas)
                {
                    GasManager.Instance.RemoveGas(gasPerJump);
                    ExpellGas();
                }
                
                base.JumpStart();
                return;
            }

            //check for second jump gas level and abort if low
            if (gasPerDoubleJump > GasManager.Instance.currentGasLevel)
            {
                GasManager.Instance.PlayEmptySFX();
                return;
            }

            //this is the second jump
            if (NumberOfJumpsLeft == 1 )
            {
                GasManager.Instance.RemoveGas(gasPerDoubleJump);
                ExpellGas();
                DoubleJump();
                base.JumpStart();
            }
        }

    }

    public void ExpellGas()
    {
        expelGasFeedbacks?.PlayFeedbacks();
    }

    public void DoubleJump()
    {
        doubleJumpFeedbacks?.PlayFeedbacks();
    }

}
