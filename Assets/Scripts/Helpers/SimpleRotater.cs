﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleRotater : MonoBehaviour
{
    public float speed = 2f;
    public bool spinCounterclockwise = false;

    protected float direction = 1;

    // Start is called before the first frame update
    void Start()
    {
        if(spinCounterclockwise)
        {
            direction = -1f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.back,speed/100/ Time.deltaTime*direction);
    }
}
