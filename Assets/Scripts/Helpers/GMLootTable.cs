using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

[System.Serializable]
public class GMLootTable : MMLootTable<MMLootGameObject, GameObject>
{
    public override MMLootGameObject GetLoot()
    {
        if (ObjectsToLoot == null)
        {
            return null;
        }

        if (ObjectsToLoot.Count == 0)
        {
            return null;
        }

        foreach (MMLootGameObject lootDropItem in ObjectsToLoot)
        {

            Debug.Log("total loot: "+ObjectsToLoot.Count);
            return lootDropItem;
           
        }
        return null;
    }

    public virtual List<MMLootGameObject> GetLootList()
    {
        return ObjectsToLoot;
    }
}
