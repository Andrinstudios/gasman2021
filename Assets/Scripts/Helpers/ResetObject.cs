﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;


/// <summary>
/// this class will reset an object animator and AIBrain on player respawn
/// </summary>
public class ResetObject : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{
    public string animationTrigger;
    public string initialBrainState;

   
    

    protected AIBrain brain;

    void Start()
    {
        brain = GetComponentInParent<AIBrain>();
    }

    void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    void OnDisable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }


    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {
        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.Respawn:

                ObjectReset();
                break;
            default:
                break;

        }
    }

    void ObjectReset()
    {
        Debug.Log(gameObject.name + " reset");
        // reset the animator to the inital state

        GetComponent<Animator>().ResetTrigger(animationTrigger);
        GetComponent<Animator>().SetTrigger("Reset");

        // reset the Brain to the inital state
        if (initialBrainState == null) { return; }

       // GetComponentInParent<AIBrain>().TransitionToState(initialBrainState);
        brain.TransitionToState(brain.States[0].StateName);

    }

}
