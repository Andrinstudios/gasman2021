﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using Cinemachine;


public class CameraZoomInOut : MonoBehaviour
{

    public CinemachineVirtualCamera newCamera;
    public LayerMask playerMask;

    private int low = 10;
    private int high = 15;
   

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (newCamera == null)
        { return; }

        if (MMLayers.LayerInLayerMask(collision.gameObject.layer, playerMask))
        {
            SwitchCarmera();
        }
    }

    protected void SwitchCarmera()
    {

        var cmBrain = CinemachineCore.Instance.GetActiveBrain(0);
        ICinemachineCamera activeCamera = cmBrain.ActiveVirtualCamera;

        CinemachineVirtualCamera oldCamera = activeCamera as CinemachineVirtualCamera;

        newCamera.Priority = high;
        oldCamera.Priority = low;


    }
}
