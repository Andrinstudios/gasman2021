﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "GasAttack")]
public class GasAttack : ScriptableObject {


	public string attackName = "gas attack";

	public ParticleSystem particles;
	public int damage = 10;
	public int coolDown = 10;
	public AudioClip clip;
	public Vector2 forceDirection;
	public float forceAmount = 10f;
	public float gasUsed = 10f;

}
