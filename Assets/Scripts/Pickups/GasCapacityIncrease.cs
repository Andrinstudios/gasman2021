﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class GasCapacityIncrease : GMPickableItem
{
    public float maxLevelIncrease = 10;

    protected override void Pick()

    {
        GasManager.Instance.ChangeMaxGasLevel(maxLevelIncrease);  
    }

}
