﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.CorgiEngine
{



    public class GasRemovePickup : GMPickableItem
    {

        public float gasToRemove = 30f;

        protected override void Pick()
        {
            GasManager.Instance.RemoveGas(gasToRemove);
        }
    }
}
