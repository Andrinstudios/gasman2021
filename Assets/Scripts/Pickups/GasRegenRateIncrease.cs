﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class GasRegenRateIncrease : GMPickableItem
{
    public float RateIncreasePercent = 10;

    protected override void Pick()
    {
        GasManager.Instance.ChangeGasRegenRate(RateIncreasePercent);
    }

}
