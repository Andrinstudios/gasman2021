﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.InventoryEngine;
using MoreMountains.Feedbacks;
using MoreMountains.CorgiEngine;

public class GMPickableItem : PickableItem, MMEventListener<CorgiEngineEvent>
{
    // Start is called before the first frame update
    public bool isRepawnable = true;

    public virtual void OnMMEvent(CorgiEngineEvent thisEvent)
    {
        switch (thisEvent.EventType)
        {
            case CorgiEngineEventTypes.Respawn:

                if (isRepawnable)
                {
                    RespawnItem();
                    
                }
                else
                {
                    Destroy(this.gameObject);
                }
                
                break;

            //case CorgiEngineEventTypes.PlayerDeath:
                
            //    //we shold not respwan this item with player respawn (likely because it was a drop)
            //   Debug.Log("player death");

            //    _renderer.enabled = false;
            //    _collider.enabled = false;
                
            //    this.gameObject.SetActive(false);
            //    break;
            default:
                break;
        }
    }

    protected void DeactivateItem()
    {
        this.gameObject.SetActive(false);
        _collider.enabled = false;
        _renderer.enabled = false;

    }

    public void RespawnItem()
    {
        this.gameObject.SetActive(true);
        _collider.enabled = true;
        _renderer.enabled = true;
    }


    private void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
        
    }
}
