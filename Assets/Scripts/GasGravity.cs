﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasGravity : MonoBehaviour {


    public float fadeTime = 10.0f;
    public float speed = 50;
    float startTime;


    Rigidbody2D rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent< Rigidbody2D > ();
        rb.AddForce(new Vector2(speed, 0));

        //need to randomize the speeds, drag, scale, diretion, gravity


        startTime = Time.time;
      //  StartCoroutine(Fade());

	}
	
	// Update is called once per frame
	void Update () {
        

	}

    IEnumerator Fade()
    {
       // Debug.Log("invoked");
        yield return new WaitForSeconds(3f);
       // Debug.Log("waited");
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        Color thisColor = sr.color;
        float t = Time.deltaTime / fadeTime;
       // Debug.Log("t = "+t);
        sr.color = new Color(thisColor.r, thisColor.g, thisColor.b, Mathf.SmoothStep(1, 0, t));
      //  Debug.Log("done");

    }

}
