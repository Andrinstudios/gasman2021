﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;
using UnityEngine.Events;
using UnityEngine.Experimental.Rendering.Universal;


public class FabreezeJet : MonoBehaviour
{

    public LayerMask _collisionMask;
    public float gasRemovalAmount;
    public MMFeedbacks cloudFeedbacks;
    public MMFeedbacks jetFeedbacks;
    public MMFeedbacks playerFeedbaks;
    public UnityEvent playerCollision;

    public float TimeOn = 3f;
    public float TimeBetweenOn = 1f;
    private float jetTimer;
    private bool isFiring = false;
    private bool isSwtichedOff = false;
    public bool isSwitchControlled = false;

    private Light2D jetLight;
    

    // Start is called before the first frame update
    void Start()
    {
        jetTimer = TimeBetweenOn;
        StartCoroutine(StartJets());
        jetLight = GetComponentInChildren<Light2D>();
    }

    // Update is called once per frame
    void Update()
    {
        jetTimer -= Time.deltaTime;
        if (jetTimer < 0 && !isFiring && !isSwtichedOff)
        {
            StartCoroutine(StartJets());
        }
    }

    public void SwitchControlActivated()
    {
        Debug.Log("switch activated");
        isSwtichedOff = true;
        ParticleSystem _jets = GetComponentInChildren<ParticleSystem>();
        if (_jets != null)
        {
            _jets.Stop();
            BoxCollider2D _collider = GetComponent<BoxCollider2D>();
            _collider.enabled = false;
            isFiring = false;
            jetLight.enabled = false;
        }
    }

    public void SwitchControlDeactivated()
    {
        Debug.Log("switch deactivated");
        isSwtichedOff = false;
        jetTimer = TimeBetweenOn;
        StartCoroutine(StartJets());
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (!isActiveAndEnabled)
		{
			return;
		}

		//check that object is in the mask
		int layer = other.gameObject.layer;
		if (_collisionMask != (_collisionMask | (1 << layer)))
		{
			return;
		}
        // we are in the mask now dtermine what we are colliding with
        if (other.tag == "Cloud")
        {
            /* this is a playerprojectile so we need to eliminate it
            -  play a sound
            -  play a paritcle effect
            -  destroy the cloud
            */
            //get a reference to the cloud that is colliding
            CloudProjectile _cloud = other.gameObject.GetComponent<CloudProjectile>();
		    if (_cloud == null)
		    {
			_cloud = other.gameObject.GetComponentInParent<CloudProjectile>();
		    }

          //  cloudFeedbacks?.UnityEngine.PlayerLoop.Initialization(_cloud.gameObject);
            cloudFeedbacks?.PlayFeedbacks(_cloud.gameObject.transform.position);
            _cloud.Destroy();

        }

        if (other.tag == "Player")
        {
            /* this is the player
                - need to decresase the gas in his tank(GasManager)
                - play a sound/effect(use feedbacks)
                - update the UI
            */


            GasManager.Instance.RemoveGas(gasRemovalAmount);
          //  playerFeedbaks?.UnityEngine.PlayerLoop.Initialization(this.gameObject);
            playerFeedbaks?.PlayFeedbacks(other.transform.position);


            //call the event associated with the player
            if (playerCollision != null)
            {
                playerCollision.Invoke();
            }

        }
    }

    protected IEnumerator StartJets()
    {   
        //turn on the jet
        isFiring = true;
        ParticleSystem _jets = GetComponentInChildren<ParticleSystem>();
        if (_jets == null)
        {
            Debug.Log("no jets");
            yield break;
        }
        jetFeedbacks?.PlayFeedbacks(this.transform.position);
        
        _jets.Play();

        Light2D _jetLight = GetComponentInChildren<Light2D>();

        _jetLight.enabled = true;
        //get the collider and tunr it on
        BoxCollider2D _collider = GetComponent<BoxCollider2D>();
        _collider.enabled = true;


         yield return new WaitForSeconds(TimeOn);
        
         //turn off the jet and start the delay timer.
        _jets.Stop();
        jetFeedbacks?.StopFeedbacks();
        _jetLight.enabled = false;
        jetTimer = TimeBetweenOn;
        isFiring = false;
        _collider.enabled = false;

    }
}
