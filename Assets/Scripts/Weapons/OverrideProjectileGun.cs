﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using System;
using MoreMountains.CorgiEngine;


    [AddComponentMenu("Corgi Engine/Weapons/ Gas Projectile Weapon")]
    /// <summary>
    /// A weapon class aimed specifically at allowing the creation of various projectile weapons, from shotgun to machine gun, via plasma gun or rocket launcher
    /// </summary>
    public class OverrideProjectileGun : Weapon
    {
        [Header("Audio")]
        public List<AudioClip> FartSounds;
        public List<AudioClip> Grunts;

        [Header("Spawn")]
        /// the offset position at which the projectile will spawn
        public Vector3 ProjectileSpawnOffset = Vector3.zero;

        public MMObjectPooler ObjectPooler { get; set; }
      //  protected WeaponAim _aimableWeapon;
        protected Vector3 _spawnPosition = Vector3.zero;

        [Header("Fire Behavior")]
        public bool onlyFireWhileGrounded = false;
        public float kickBackPower = 5;
        public Vector2 kickbackDirection;

        [Header("Charging")]
        public bool requiresCharging = false;
        public float chargingTime = 3f;
        private bool isCharging = false;
        private float startTime = 0;
        private bool isPlayingSound = false;

        /// <summary>
        /// Initialize this weapon
        /// </summary>
        public override void Initialization()
        {
            base.Initialization();

            _aimableWeapon = GetComponent<WeaponAim>();

            if (GetComponent<MMMultipleObjectPooler>() != null)
            {
                ObjectPooler = GetComponent<MMMultipleObjectPooler>();
            }
            if (GetComponent<MMSimpleObjectPooler>() != null)
            {
                ObjectPooler = GetComponent<MMSimpleObjectPooler>();
            }
            if (ObjectPooler == null)
            {
                Debug.LogWarning(this.name + " : no object pooler (simple or multiple) is attached to this Projectile Weapon, it won't be able to shoot anything.");
                return;
            }
        }

        public override void WeaponInputStart()
        {
            // does weapon require charging time?
            // if yes then freeze the character and start timer as long as button held down.
            //  
            // if no then procede to fire the weapon.
            


            if (onlyFireWhileGrounded)
            {
                CorgiController _con = Owner.GetComponent<CorgiController>();
                if (!_con.State.IsGrounded) { return; }
            }

            base.WeaponInputStart();
        }

        protected override void ShootRequest()
        {

            // get the object bieng shot and find the gas used per shot be validate firign is possible
            GameObject pooledObject = ObjectPooler.GetPooledGameObject();
            CloudProjectile cloud = pooledObject.GetComponent<CloudProjectile>();

            if (cloud == null) { return; }

            float gasNeededToFire = cloud.gasUsedPerProjectile;
             
            if (GasManager.Instance.currentGasLevel > gasNeededToFire )  
            {
                WeaponState.ChangeState(WeaponStates.WeaponUse);
            }else{
                Debug.Log("Out of Gas!!!");
                //TODO  augment this to play a sound and animation when gsa level is too low.
            }
        }

        /// <summary>
        /// Called everytime the weapon is used
        /// </summary>
        protected override void WeaponUse()
        {
            base.WeaponUse();
            KickBack();
            DetermineSpawnPosition();

            /*_spawnPosition = this.transform.localPosition + this.transform.localRotation * ProjectileSpawnOffset;
            _spawnPosition = this.transform.TransformPoint (_spawnPosition);*/

            SpawnProjectile(_spawnPosition);
        }

        /// <summary>
        /// Spawns a new object and positions/resizes it
        /// </summary>
        public virtual GameObject SpawnProjectile(Vector3 spawnPosition, bool triggerObjectActivation = true)
        {
            /// we get the next object in the pool and make sure it's not null
            GameObject nextGameObject = ObjectPooler.GetPooledGameObject();

            // mandatory checks
            if (nextGameObject == null) { return null; }
            if (nextGameObject.GetComponent<MMPoolableObject>() == null)
            {
                throw new Exception(gameObject.name + " is trying to spawn objects that don't have a PoolableObject component.");
            }
            // we position the object
            nextGameObject.transform.position = spawnPosition;
            // we set its direction

            Projectile projectile = nextGameObject.GetComponent<Projectile>();
            if (projectile != null)
            {
                projectile.SetWeapon(this);
                projectile.SetOwner(Owner.gameObject);
            }
            // we activate the object
            nextGameObject.gameObject.SetActive(true);


            if (projectile != null)
            {
                projectile.SetDirection(transform.right * (Flipped ? -1 : 1), transform.rotation, Owner.IsFacingRight);
            }

            if (triggerObjectActivation)
            {
                if (nextGameObject.GetComponent<MMPoolableObject>() != null)
                {
                    nextGameObject.GetComponent<MMPoolableObject>().TriggerOnSpawnComplete();
                }
            }

            return (nextGameObject);
        }

        /// <summary>
        /// Determines the spawn position based on the spawn offset and whether or not the weapon is flipped
        /// </summary>
        protected virtual void DetermineSpawnPosition()
        {
            if (Flipped)
            {
                _spawnPosition = this.transform.position - this.transform.rotation * ProjectileSpawnOffset;
            }
            else
            {
                _spawnPosition = this.transform.position + this.transform.rotation * ProjectileSpawnOffset;
            }
        }

        /// <summary>
        /// When the weapon is selected, draws a circle at the spawn's position
        /// </summary>
        protected virtual void OnDrawGizmosSelected()
        {
            DetermineSpawnPosition();

            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(_spawnPosition, 0.2f);
        }

        /// <summary>
        /// moves the player in the opposite direction of the gasBlast.
        /// </summary>
        protected virtual void KickBack()
        {
            CorgiController _controller = Owner.GetComponent<CorgiController>();
            //get the direction the player is facing and set the force in the x direction to match.
            float playerFacingDirection = transform.right.x * (Flipped ? -1 : 1);
            Vector2 kickBackForce = new Vector2(kickBackPower * playerFacingDirection * kickbackDirection.x, kickbackDirection.y * kickBackPower);

            _controller.AddForce(kickBackForce);

            //TODO  add call to update anmimator

        }

        protected override void TriggerWeaponStartFeedback()
        {
            /*if (isPlayingSound){
                Debug.Log("is playing = true");
                return;
            } 
            */
            if (WeaponStartMMFeedback!=null)
            {   
            //    AudioClip grunt = Grunts[UnityEngine.Random.Range(0,Grunts.Count)];
            //    GMSoundManager.Instance.PlaySound(grunt,transform.position);
            // isPlayingSound = true;
            // Invoke("ResetIsSounding",WeaponStartSfx.length/2.2f);

                 WeaponStartMMFeedback?.PlayFeedbacks();
            }
        }

        protected override void TriggerWeaponUsedFeedback()
        {
        // AudioClip fart = FartSounds[UnityEngine.Random.Range(0,FartSounds.Count)];
        // GMSoundManager.Instance.PlaySound(fart,transform.position);
             WeaponUsedMMFeedback?.PlayFeedbacks();
        }

        private void ResetIsSounding()
        {
            isPlayingSound = false;
            Debug.Log("is playing = false");
        }

    }


