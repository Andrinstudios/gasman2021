﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{

    public class AtomicGasGun : OverrideProjectileGun
    {

        protected override void ProcessWeaponState()
        {
            if (WeaponState == null) { return; }

            switch (WeaponState.CurrentState)
            {
                case WeaponStates.WeaponStart:
                    if (DelayBeforeUse > 0)
                    {
                        _delayBeforeUseCounter = DelayBeforeUse;
                        WeaponState.ChangeState(WeaponStates.WeaponDelayBeforeUse);
                    }
                    else
                    {
                        ShootRequest();
                    }
                    break;

                case WeaponStates.WeaponDelayBeforeUse:
                    _delayBeforeUseCounter -= Time.deltaTime;
                    if (_delayBeforeUseCounter <= 0)
                    {
                        ShootRequest();
                    }
                    break;

                case WeaponStates.WeaponUse:
                    WeaponUse();
                    _delayBetweenUsesCounter = TimeBetweenUses;
                    WeaponState.ChangeState(WeaponStates.WeaponDelayBetweenUses);
                    break;

                case WeaponStates.WeaponDelayBetweenUses:
                    _delayBetweenUsesCounter -= Time.deltaTime;
                    if (_delayBetweenUsesCounter <= 0)
                    {
                        if ((TriggerMode == TriggerModes.Auto) && !_triggerReleased)
                        {
                            ShootRequest();
                        }
                        else
                        {
                            TurnWeaponOff();
                        }
                    }
                    break;

                case WeaponStates.WeaponStop:
                    WeaponState.ChangeState(WeaponStates.WeaponIdle);
                    break;

                case WeaponStates.WeaponReloadNeeded:
                    ReloadNeeded();
                    WeaponState.ChangeState(WeaponStates.WeaponIdle);
                    break;

                case WeaponStates.WeaponReloadStart:
                    ReloadWeapon();
                    _reloadingCounter = ReloadTime;
                    WeaponState.ChangeState(WeaponStates.WeaponReload);
                    break;

                case WeaponStates.WeaponReload:
                    _reloadingCounter -= Time.deltaTime;
                    if (_reloadingCounter <= 0)
                    {
                        WeaponState.ChangeState(WeaponStates.WeaponReloadStop);
                    }
                    break;

                case WeaponStates.WeaponReloadStop:
                    _reloading = false;
                    WeaponState.ChangeState(WeaponStates.WeaponIdle);
                    if (WeaponAmmo == null)
                    {
                        CurrentAmmoLoaded = MagazineSize;
                    }
                    break;
            }
        }


        public override void WeaponInputStart()
        {
            base.WeaponInputStart();
        }

    }
}
