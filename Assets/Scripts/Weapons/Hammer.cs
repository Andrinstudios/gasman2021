﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class Hammer : MeleeWeapon
{
    public override void Initialization()
    {
        if (!_initialized)
        {
            Flipped = false;
            _spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
            _comboWeapon = this.gameObject.GetComponent<ComboWeapon>();
            WeaponState = new MMStateMachine<WeaponStates>(gameObject, true);
            _aimableWeapon = GetComponent<WeaponAim>();
            WeaponAmmo = GetComponent<WeaponAmmo>();
            _animatorParameters = new List<HashSet<int>>();
            InitializeAnimatorParameters();
            InitializeFeedbacks();
            _initialized = true;
        }
        WeaponState.ChangeState(WeaponStates.WeaponIdle);
        if (WeaponAmmo == null)
        {
            CurrentAmmoLoaded = MagazineSize;
        }

        if (_characterHorizontalMovement != null)
        {
            if (_characterHorizontalMovement.MovementSpeedMultiplier == 0f)
            {
                _characterHorizontalMovement.MovementSpeedMultiplier = 1f;
            }
            _permanentMovementMultiplierStorage = _characterHorizontalMovement.MovementSpeedMultiplier;
        }

    }

    protected override void WeaponUse()
    {
        base.WeaponUse();
    }

    protected override IEnumerator MeleeWeaponAttack()
    {
        if (_attackInProgress) { yield break; }

        _attackInProgress = true;
        yield return new WaitForSeconds(InitialDelay);
       // EnableDamageArea();
        yield return new WaitForSeconds(ActiveDuration);
       // DisableDamageArea();
        _attackInProgress = false;
    }
}
