﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillGas : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "GasCloud")
        {
            Destroy(collision.gameObject);
        }
    }
}
