using System;
using UnityEngine;

namespace ES3Types
{
	[UnityEngine.Scripting.Preserve]
	[ES3PropertiesAttribute("objectID", "CurrentHealth", "InitialHealth", "MaximumHealth", "_initialPosition")]
	public class ES3UserType_GMHealth : ES3ComponentType
	{
		public static ES3Type Instance = null;

		public ES3UserType_GMHealth() : base(typeof(GMHealth)){ Instance = this; priority = 1;}


		protected override void WriteComponent(object obj, ES3Writer writer)
		{
			var instance = (GMHealth)obj;
			
			writer.WriteProperty("objectID", instance.objectID, ES3Type_string.Instance);
			writer.WriteProperty("CurrentHealth", instance.CurrentHealth, ES3Type_int.Instance);
			writer.WriteProperty("InitialHealth", instance.InitialHealth, ES3Type_int.Instance);
			writer.WriteProperty("MaximumHealth", instance.MaximumHealth, ES3Type_int.Instance);
			writer.WritePrivateField("_initialPosition", instance);
		}

		protected override void ReadComponent<T>(ES3Reader reader, object obj)
		{
			var instance = (GMHealth)obj;
			foreach(string propertyName in reader.Properties)
			{
				switch(propertyName)
				{
					
					case "objectID":
						instance.objectID = reader.Read<System.String>(ES3Type_string.Instance);
						break;
					case "CurrentHealth":
						instance.CurrentHealth = reader.Read<System.Int32>(ES3Type_int.Instance);
						break;
					case "InitialHealth":
						instance.InitialHealth = reader.Read<System.Int32>(ES3Type_int.Instance);
						break;
					case "MaximumHealth":
						instance.MaximumHealth = reader.Read<System.Int32>(ES3Type_int.Instance);
						break;
					case "_initialPosition":
					reader.SetPrivateField("_initialPosition", reader.Read<UnityEngine.Vector3>(), instance);
					break;
					default:
						reader.Skip();
						break;
				}
			}
		}
	}


	public class ES3UserType_GMHealthArray : ES3ArrayType
	{
		public static ES3Type Instance;

		public ES3UserType_GMHealthArray() : base(typeof(GMHealth[]), ES3UserType_GMHealth.Instance)
		{
			Instance = this;
		}
	}
}