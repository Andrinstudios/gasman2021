﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;


public class FallingTrigger : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{

    public FallingObject fallingObject;
    public LayerMask triggerMask;
    public MMFeedbacks TriggeredFeedbacks;
    protected bool isTriggered = false;

    protected Collider2D triggerCollider;

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    protected virtual void Initialization()
    {
        triggerCollider = this.gameObject.GetComponent<Collider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (MMLayers.LayerInLayerMask(collision.gameObject.layer, triggerMask))
        {
            fallingObject.StartFall();
            triggerCollider.enabled = false;
            isTriggered = true;
            TriggeredFeedbacks?.PlayFeedbacks();
        }
    }


    private void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }

    public void OnMMEvent(CorgiEngineEvent cEvent)
    {
        if (cEvent.EventType == CorgiEngineEventTypes.Respawn)
        {
            Reset();
        }
    }

    protected virtual void Reset()
    {
        triggerCollider.enabled = true;
        fallingObject.gameObject.SetActive(true);
        
    }
}
